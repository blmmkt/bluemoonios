//
//  SaleViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Toaster
import Firebase

class SaleViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITabBarDelegate {
    
    @IBOutlet weak var saleTitleBack: UIView!
    @IBOutlet weak var saleCollectionView: UICollectionView!
    @IBOutlet weak var saleBottomNav: UITabBar!
    
    var saleDataList: Array<ResSales.Sale>! = []
    var pageNum: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setWidgets()
        getSaleData()
    }
    
    func setWidgets() {
//        let gradient = CAGradientLayer()
//        gradient.startPoint = CGPoint(x: 0, y: 0.5)
//        gradient.endPoint = CGPoint(x: 1, y: 0.5)
//        gradient.colors = [
//            UIColor(red: 40/255, green: 42/255, blue: 80/255, alpha: 1).cgColor,
//            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
//        ]
//        gradient.locations = [0, 1]
//        gradient.frame = saleTitleBack.bounds
//        saleTitleBack.layer.addSublayer(gradient)
        
        saleCollectionView.delegate = self
        saleCollectionView.dataSource = self
        
        saleBottomNav.delegate = self
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return saleDataList.count == 0 ? 1 : saleDataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "saleCell", for: indexPath) as! SaleCollectionViewCell
        
        if saleDataList.count == 0 {
            cell.imageView.image = UIImage(named: "icon_empty")
            cell.progress.isHidden = true
            cell.salesPercent.isHidden = true
            cell.likeImg.isHidden = true
            cell.timerText.isHidden = true
            cell.brand.text = NSLocalizedString("no_item", comment: "")
            cell.product.isHidden = true
            cell.originPrice.isHidden = true
            cell.discountRate.isHidden = true
            cell.price.isHidden = true
            
            return cell
        } else {
            cell.progress.isHidden = false
            cell.salesPercent.isHidden = false
            cell.likeImg.isHidden = false
            cell.timerText.isHidden = false
            cell.product.isHidden = false
            cell.price.isHidden = false
        
            let comingData = self.saleDataList[indexPath.row]
            
            let oriUrl = comingData.imgPath + comingData.thumbnails.split(separator: "|")[0]
            let kor = oriUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let urlKor = URL(string: kor)
            
            cell.imageView.kf.setImage(with: urlKor)
            
            cell.progress.setSecondaryProgress(progress: Float(comingData.minimum / comingData.maximum))
            cell.progress.setProgress(Float(comingData.sales / comingData.maximum), animated: true)
            
            cell.likeImg.tag = indexPath.row + 1
            
            if comingData.my_like {
                cell.likeImg.setImage(UIImage(named: "icon_wish"), for: .normal)
                
                cell.likeImg.tag *= -1
            } else {
                cell.likeImg.setImage(UIImage(named: "icon_wish_empty"), for: .normal)
            }
            
//            cell.salesPercent.text = String(Int(Float(comingData.sales / comingData.minimum) * 100)) + "%"
            
            cell.salesPercent.text = String.init(format: "%.0f", comingData.maximum) + " left"
            
            if comingData.sales / comingData.minimum > 1 {
                cell.salesPercent.textColor = UIColor.red
            }
            
            cell.brand.text = comingData.manufacturer
            cell.product.text = comingData.name
            
            if(comingData.discount == "Y") {
                if comingData.discount_rate > 0 {
                    cell.setOriginPrice(text: UtilMethod.getForattedPrice(price: UtilMethod.getOriginPrice(priceString: comingData.price, discount_rate: comingData.discount_rate)) + " " + UtilMethod.getLocalUnit())
                    
                    cell.price.text = UtilMethod.getForattedPrice(price: UtilMethod.getLocalPrice(priceString: comingData.price)) + " " + UtilMethod.getLocalUnit()
                    cell.discountRate.text = String(comingData.discount_rate) + " %"
                    cell.originPrice.isHidden = false
                    cell.discountRate.isHidden = false
                } else {
                    cell.originPrice.isHidden = true
                    cell.discountRate.isHidden = true
                }
            } else {
                cell.price.text = UtilMethod.getForattedPrice(price: UtilMethod.getLocalPrice(priceString: comingData.price)) + " " + UtilMethod.getLocalUnit()
                cell.originPrice.isHidden = true
                cell.discountRate.isHidden = true
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            if let date = dateFormatter.date(from: comingData.sale_end) {
                cell.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                    cell.timerText.text = date.countDownFormat
                    if Date() > date {
                        timer.invalidate()
                    }
                })
                cell.timer?.fire()
                RunLoop.current.add(cell.timer!, forMode: .common)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if saleDataList.count > 0 {
            let sale = saleDataList[indexPath.row]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "detailComingSoonViewController") as! DetailComingSoonViewController
            vc.product = sale
            navigationController?.pushViewController(vc, animated: true)
            
            Analytics.logEvent("DetailView", parameters: [
                "detail": "sale" as NSObject
                ])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 24
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/2, height: 340)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == saleDataList.count - 1 {
            getSaleDataMore10()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "saleOffice", for: indexPath)
        
        return footer
    }
    
    @IBAction func saleLikeAction(_ sender: UIButton) {
        if sender.tag < 0 {
            dislikeAction(position: (-1) * (sender.tag + 1))
        } else {
            likeAction(position: sender.tag - 1)
        }
    }
    
    func getSaleData() {
        Alamofire.request(
            Constants.BASE_URL + "Product_Sale_List.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "page": 0],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResSales>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.saleDataList = result.sales_list
                        
                        self.saleCollectionView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func getSaleDataMore10() {
        Alamofire.request(
            Constants.BASE_URL + "Product_Sale_List.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "page": self.pageNum],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResSales>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.saleDataList.append(contentsOf: result.sales_list)
                        
                        self.saleCollectionView.reloadData()
                        
                        self.pageNum+=1
                    } else if result.error_msg == "" {
                        Toast(text: NSLocalizedString("last_item", comment: ""), delay: 0, duration: 2.0).show()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func likeAction(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Ok.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(saleDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.saleDataList[position].my_like = true
                        
                        self.saleCollectionView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func dislikeAction(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Cancle.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(saleDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.saleDataList[position].my_like = false
                        
                        self.saleCollectionView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            break
        case 1:
            self.performSegue(withIdentifier: "unwindSaleToMain", sender: self)
            self.performSegue(withIdentifier: "saleToCart", sender: self)
            break
        case 2:
            self.performSegue(withIdentifier: "unwindSaleToMain", sender: self)
            self.performSegue(withIdentifier: "saleToMyPage", sender: self)
            break
        default:
            break
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
