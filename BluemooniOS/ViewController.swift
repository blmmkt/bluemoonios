//
//  ViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 14/03/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        checkLogin()
    }
    
    func checkLogin() {
        if Constants.USER_DEFAULT.bool(forKey: "login") {
            checkFbOrNot()
        } else {
            guestLogin()
        }
    }
    
    func guestLogin() {
        Alamofire.request(
            Constants.BASE_URL + "Guest_Insert_Ok.jsp",
            method: .post,
            parameters: [:],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<ResGuest>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        Constants.USER_DEFAULT.set(result.user_id, forKey: "id")
                        Constants.USER_DEFAULT.set(result.token, forKey: "tkn")

                        self.goToMain()
                    } else {
                        self.showAlert(title: "Error", message: "Server error occurred. Please try again")
                    }
                }
        }
    }
    
    func checkFbOrNot() {
        if(Constants.USER_DEFAULT.bool(forKey: "fb_login")) {
            checkFbLoginState()
        } else {
            checkIdLoginState()
        }
    }
    
    func checkFbLoginState() {
        Alamofire.request(
            Constants.BASE_URL + "User_Chk_Token.jsp",
            method: .post,
            parameters: ["id": Constants.USER_DEFAULT.string(forKey: "id") ?? "",
                         "fb_token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? ""],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.goToMain()
                    } else {
                        self.guestLogin()
                    }
                }
        }
    }
    
    func checkIdLoginState() {
        Alamofire.request(
            Constants.BASE_URL + "User_Login_Ok.jsp",
            method: .post,
            parameters: ["id": Constants.USER_DEFAULT.string(forKey: "id") ?? "",
                         "pw": Constants.USER_DEFAULT.string(forKey: "pw") ?? ""],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject { (response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.goToMain()
                    } else {
                        self.guestLogin()
                    }
                }
        }
    }
    
    func goToMain() {
        self.performSegue(withIdentifier: "splashToMain", sender: self)
    }
    
//    func checkTutorial() {
//        if Constants.USER_DEFAULT.bool(forKey: "tutorial") {
//            self.performSegue(withIdentifier: "loginSegue", sender: self)
//        } else {
//            self.performSegue(withIdentifier: "splashToTutorial", sender: self)
//        }
//    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destinationViewController = segue.destination as? TutorialViewController {
//            destinationViewController.splashDelegate = self
//        }
//    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("done", comment: ""), style: .default, handler: nil)
        alertController.addAction(action)
    
        present(alertController, animated: true, completion: nil)
    }
}
