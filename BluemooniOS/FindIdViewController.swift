//
//  FindIdViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 04/05/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class FindIdViewController: UIViewController {

    @IBOutlet weak var findIdScroll: UIScrollView!
    @IBOutlet weak var findContentView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var findIdTitle: UILabel!
    @IBOutlet weak var findIdTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var findIdButton: UIButton!
    @IBOutlet weak var findPwTitle: UILabel!
    @IBOutlet weak var findPwIdTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var findPwEmailTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var findPwDatePicker: UIDatePicker!
    @IBOutlet weak var findPwButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setWidgets()
    }
    
    func setWidgets() {
        findIdTitle.text = NSLocalizedString("find_id_title", comment: "")
        
        findIdButton.setTitle(NSLocalizedString("find_id_button", comment: ""), for: .normal)
        
        findPwTitle.text = NSLocalizedString("find_pw_title", comment: "")
    }
    
    @IBAction func exitAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func findIdAction(_ sender: UIButton) {
        Alamofire.request(
            Constants.BASE_URL + "User_FindId_Ok.jsp",
            method: .post,
            parameters: ["email": findIdTextField.text!],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.showAlert("Check", message: "Check your mailbox")
                    } else {
                        self.showAlert("Error", message: "There's no ID matched with email")
                    }
                }
        }
    }
    
    @IBAction func findPwAction(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let birth = dateFormatter.string(from: findPwDatePicker.date)
        
        Alamofire.request(
            Constants.BASE_URL + "User_FindPw_Ok.jsp",
            method: .post,
            parameters: ["id": findPwIdTextField.text!,
                         "email": findPwEmailTextField.text!,
                         "birthday": birth],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFindPw>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "updatePwStory") as! UpdatePwViewController
                        vc.token = result.token
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        self.showAlert("Error", message: "Something is not matched")
                    }
                }
        }
    }
    
    @IBAction func edgeScreen(_ sender: UIScreenEdgePanGestureRecognizer) {
        if sender.state == .recognized {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlert(_ title: String, message: String) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: NSLocalizedString("dialog_close", comment: ""), style: .default, handler: nil)
        alert.addAction(dismissAction)
        present(alert, animated: true, completion: nil)
    }
}
