//
//  UpdatePwViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 11/05/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Toaster

class UpdatePwViewController: UIViewController {

    @IBOutlet weak var updatePwTitle: UILabel!
    @IBOutlet weak var updatePwTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var updatePwTextFieldAgain: UnderlineTextFieldBlack!
    @IBOutlet weak var updatePwOk: UIButton!
    
    var token: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setWidgets()
    }
    
    func setWidgets() {
        updatePwTitle.text = NSLocalizedString("new_pw", comment: "")
        
        updatePwOk.setTitle(NSLocalizedString("sign_up_submit", comment: ""), for: .normal)
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updatePwAction(_ sender: UIButton) {
        Alamofire.request(
            Constants.BASE_URL + "User_UpdatePw_Ok.jsp",
            method: .post,
            parameters: ["token": self.token,
                         "pw": updatePwTextField.text!],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFindPw>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        Toast(text: NSLocalizedString("done", comment: ""), delay: 0, duration: 2.0).show()
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController.init(title: "Error", message: NSLocalizedString("pw_invalid", comment: ""), preferredStyle: .alert)
                        let action = UIAlertAction.init(title: "complete_positive", style: .cancel, handler: nil)
                    }
                }
        }
    }
}
