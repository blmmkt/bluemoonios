//
//  VersionCheckViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 19/05/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class VersionCheckViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        
        Alamofire.request(
            Constants.BASE_URL + "Version_Chk.jsp",
            method: .post,
            parameters: ["os": "iOS",
                         "version": version],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResVersionChk>) in
                if let result = response.result.value {
                    if result.update {
                        let alert = UIAlertController.init(title: "New Version Appeared", message: "Will you update?", preferredStyle: .alert)
                        let dismissAction = UIAlertAction.init(title: "No", style: .cancel, handler: { (UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        })
                        
                        var updateAction: UIAlertAction
                        
                        if result.force {
                            updateAction = UIAlertAction.init(title: "Update", style: .destructive, handler: { (UIAlertAction) in
                                if let url = URL(string: "https://itunes.apple.com/vn/app/apple-store/id1462280145"),
                                    UIApplication.shared.canOpenURL(url)
                                {
                                    if #available(iOS 10.0, *) {
                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    } else {
                                        UIApplication.shared.openURL(url)
                                    }
                                }
                            })
                        } else {
                            updateAction = UIAlertAction.init(title: "Update", style: .default, handler: { (UIAlertAction) in
                                if let url = URL(string: "https://itunes.apple.com/vn/app/apple-store/id1462280145"),
                                    UIApplication.shared.canOpenURL(url)
                                {
                                    if #available(iOS 10.0, *) {
                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    } else {
                                        UIApplication.shared.openURL(url)
                                    }
                                }
                            })
                            
                            alert.addAction(dismissAction)
                        }
                        
                        alert.addAction(updateAction)
                        
                        self.present(alert, animated: true)
                    }
                }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
