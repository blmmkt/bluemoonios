//
//  ResSignIn.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 14/04/2019.
//

import ObjectMapper

class ResSignIn: Mappable {
    var error_msg: String!
    var token: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        token <- map["token"]
    }
}
