//
//  Product.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import UIKit

class Product {
    var id: String!
    var name: String!
    var manufacturer: String!
    var price: String!
    var imgPath: String!
    var thumbnails: String!
    var discount: String!
    var discount_rate: Int!
    var sales: Float!
    var sold_out: Bool!
    var my_like: Bool!
    var like: Int!
    var minimum: Float!
    var maximum: Float!
    var description_text1: String!
    var description_text2: String!
    var description_img: String!
    
    var currentPrice: Float {
        return UtilMethod.getLocalPrice(priceString: price)
    }
    
    var originalPrice: Float {
        return UtilMethod.getOriginPrice(priceString: price, discount_rate: discount_rate)
    }
    
    var currentPriceText: NSMutableAttributedString {
        let medium: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 23, weight: .medium)]
        let bold: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 23, weight: .bold)]
        let formattedString =
            NSMutableAttributedString()
//        formattedString.append(NSMutableAttributedString(string: "BLM Price ", attributes: medium))
        formattedString.append(NSMutableAttributedString(string: "\(currentPrice)", attributes: bold))
        formattedString.append(NSMutableAttributedString(string: " " + UtilMethod.getLocalUnit(), attributes: medium))
        return formattedString
    }
    
    var originalPriceText: NSMutableAttributedString? {
        if self is ResTease.Tease {
            return nil
        } else {
            let strike: [NSAttributedString.Key: Any] = [.strikethroughStyle: 1]
            let medium: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 23, weight: .medium)]
            let formattedString = NSMutableAttributedString()
            formattedString.append(NSMutableAttributedString(string: "\(originalPrice)", attributes: strike))
            formattedString.append(NSMutableAttributedString(string: " " + UtilMethod.getLocalUnit(), attributes: medium))
            return formattedString
        }
    }
    
    func totalPriceText(quantity: Int) -> NSMutableAttributedString {
        let medium: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 23, weight: .medium)]
        let bold: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 23, weight: .bold)]
        let formattedString = NSMutableAttributedString()
        formattedString.append(NSMutableAttributedString(string: "\(currentPrice * Float(quantity))", attributes: bold))
        formattedString.append(NSMutableAttributedString(string: " " + UtilMethod.getLocalUnit(), attributes: medium))
        return formattedString
    }
    
}
