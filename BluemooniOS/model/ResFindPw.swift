//
//  ResFindPw.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 11/05/2019.
//

import ObjectMapper

class ResFindPw: Mappable {
    var error_msg: String = ""
    var token: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        token <- map["token"]
    }
    

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
