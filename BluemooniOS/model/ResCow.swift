//
//  ResCow.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 29/04/2019.
//

import ObjectMapper

class ResCow: Mappable {
    
    var error_msg: String!
    var sales_list: Array<Cow>!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        sales_list <- map["sales_list"]
    }
    
    class Cow: Product, Mappable {
        
        var success: Bool!
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            id <- map["id"]
            name <- map["name"]
            manufacturer <- map["manufacturer"]
            price <- map["price"]
            imgPath <- map["imgPath"]
            thumbnails <- map["thumbnails"]
            discount <- map["discount"]
            discount_rate <- map["discount_rate"]
            sales <- map["sales"]
            sold_out <- map["sold_out"]
            like <- map["like"]
            my_like <- map["my_like"]
            minimum <- map["minimum"]
            maximum <- map["maximum"]
            success <- map["success"]
            description_text1 <- map["description_text1"]
            description_text2 <- map["description_text2"]
            description_img <- map["description_img"]
        }
    }
}
