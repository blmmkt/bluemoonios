//
//  ResGuest.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 24/06/2019.
//

import ObjectMapper

class ResGuest: Mappable {
    var error_msg: String!
    var user_id: String!
    var token: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        user_id <- map["user_id"]
        token <- map["token"]
    }
}
