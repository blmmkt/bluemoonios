//
//  CartToTease.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 18/05/2019.
//
import Foundation

class CartToTease: Product {
    var sale_end: String!
    
    init(mId: String, mName: String, mManufacturer: String, mPrice: String, mImagePath: String, mthumbnails: String, mDiscount: String, mDiscountRate: Int, mSales: Float, mSaleEnd: String, mSoldOut: Bool, mMyLike: Bool, mLike: Int, mMinimum: Float, mMaximum: Float, mDescriptionText1: String,mDescriptionText2: String,mDescriptionImg: String) {
        super.init()
        id = mId
        name = mName
        manufacturer = mManufacturer
        price = mPrice
        imgPath = mImagePath
        thumbnails = mthumbnails
        discount = mDiscount
        discount_rate = mDiscountRate
        sales = mSales
        sale_end = mSaleEnd
        sold_out = mSoldOut
        like = mLike
        my_like = mMyLike
        minimum = mMinimum
        maximum = mMaximum
        description_text1 = mDescriptionText1
        description_text2 = mDescriptionText2
        description_img = mDescriptionImg
    }
    
}
