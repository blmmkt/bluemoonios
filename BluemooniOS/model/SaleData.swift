//
//  SaleData.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 04/04/2019.
//

import UIKit

class SaleData: NSObject {
    var imgPath: String!
    var thumbnails: String!
    var name: String!
}
