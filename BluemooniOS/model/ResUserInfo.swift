//
//  ResUserInfo.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 01/05/2019.
//

import ObjectMapper

class ResUserInfo: Mappable {
    
    var error_msg: String!
    var thumbnail: String!
    var id: String!
    var first_name: String!
    var last_name: String!
    var email: String!
    var gender: String!
    var phone_num: String!
    var birthday: String!
    var rank : Int!
    var point: Int!
    var address1: String!
    var city: String!
    var province: String!
    var country: String!
    var address2: String!
    var zip_code: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        thumbnail <- map["thumbnail"]
        id <- map["id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        gender <- map["gender"]
        phone_num <- map["phone_num"]
        birthday <- map["birthday"]
        rank <- map["rank"]
        point <- map["point"]
        address1 <- map["address1"]
        city <- map["city"]
        province <- map["province"]
        country <- map["country"]
        address2 <- map["address2"]
        zip_code <- map["zip_code"]
    }
}
