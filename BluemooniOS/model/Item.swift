//
//  Item.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import Foundation

class Item {
    var seq: String
    var itemId: String
    var thumbId: String
    var price: Float
    var quantity: Int
    
    init(seq: String, itemId: String, thumbId: String, price: Float, quantity: Int) {
        self.seq = seq
        self.itemId = itemId
        self.thumbId = thumbId
        self.price = price
        self.quantity = quantity
    }
    
}
