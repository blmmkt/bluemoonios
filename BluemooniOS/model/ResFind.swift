//
//  ResFind.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 13/04/2019.
//

import ObjectMapper

class ResFind: Mappable {
    var error_msg: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
    }
}
