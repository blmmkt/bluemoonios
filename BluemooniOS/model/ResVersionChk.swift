//
//  ResVersionChk.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 19/05/2019.
//

import ObjectMapper

class ResVersionChk: Mappable {
    var mAlert: String!
    var update: Bool!
    var force: Bool!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mAlert <- map["alert"]
        update <- map["update"]
        force <- map["force"]
    }
}
