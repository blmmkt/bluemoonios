//
//  ResOrder.swift
//  BluemooniOS
//
//  Created by Soso on 05/05/2019.
//

import ObjectMapper

class ResOrder: Mappable {
    var error_msg: String = ""
    var imgPath: String = ""
    var check_out_list: [ResOrder.Order] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        imgPath <- map["imgPath"]
        check_out_list <- map["check_out_list"]
    }
    
    class Order: Mappable {
        var list_item: [ResOrder.Order.Element] = []
        var delivery: String = ""
        var total: String = ""
        var invoice_num: String = ""
        var order_id: String = ""
        
        required init?(map: Map) {}
        
        func mapping(map: Map) {
            list_item <- map["list_item"]
            delivery <- map["delivery"]
            total <- map["total"]
            invoice_num <- map["invoice_num"]
            order_id <- map["order_id"]
        }
        
        var currentTotal: String {
            var current: Float
            switch NSLocale.current.languageCode {
            case "en":
                current = Float(String(total.split(separator: "|")[1])) ?? 0
            case "ko":
                current = Float(String(total.split(separator: "|")[0])) ?? 0
            case "vi":
                current = Float(String(total.split(separator: "|")[2])) ?? 0
            default:
                current = Float(String(total.split(separator: "|")[1])) ?? 0
            }
            if delivery == "Y" {
                current += UtilMethod.getLocalDeliveryFee()
            }
            switch NSLocale.current.languageCode {
            case "en":
                return "\(current)" + " USD"
            case "ko":
                return "\(current)" + " 원"
            case "vi":
                return "\(current)" + " VND"
            default:
                return "\(current)" + " USD"
            }
        }
        
        class Element: Mappable {
            var amount: Int = 0
            var thumbnail: String = ""
            var item_id: String = ""
            var price: String = ""
            var name: String = ""
            var manufacturer: String = ""
            
            required init?(map: Map) {}
            
            func mapping(map: Map) {
                amount <- map["amount"]
                thumbnail <- map["thumbnail"]
                item_id <- map["item_id"]
                price <- map["price"]
                name <- map["name"]
                manufacturer <- map["manufacturer"]
            }
            
            var currentDetail: String {
                let array = item_id.split(separator: "_")
                let color = String(array[1])
                let size = UtilMethod.changeNumToSize(num: String(array[2]))
                return "\(size) / \(color) / \(amount) ea"
            }
            
            var currentPrice: String {
                switch NSLocale.current.languageCode {
                case "en":
                    return "\(Float(String(price.split(separator: "|")[1])) ?? 0)" + " USD"
                case "ko":
                    return "\(Float(String(price.split(separator: "|")[0])) ?? 0)" + " 원"
                case "vi":
                    return "\(Float(String(price.split(separator: "|")[2])) ?? 0)" + " VND"
                default:
                    return "\(Float(String(price.split(separator: "|")[1])) ?? 0)" + " USD"
                }
            }
            
        }
        
    }
    
}
