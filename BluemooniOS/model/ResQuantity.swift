//
//  ResQuantity.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import ObjectMapper

class ResQuantity: Mappable {
    var error_msg: String!
    var quantity: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        quantity <- map["quantity"]
    }
}
