//
//  ResSizeColor.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import ObjectMapper

class ResSizeColor: Mappable {
    var error_msg: String!
    var size: String!
    var color: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        error_msg <- map["error_msg"]
        size <- map["size"]
        color <- map["color"]
    }
}
