//
//  CartCollectionViewCell.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 29/04/2019.
//

import UIKit

class CartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var options: UILabel!
    @IBOutlet weak var price: UILabel!
    
    weak var delegate: CartDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 15
    }
    
    @IBAction func editPressed(_ sender: UIButton) {
        delegate.editPressed(self)
    }
    
    @IBAction func deletePressed(_ sender: UIButton) {
        delegate.deletePressed(self)
    }
    
}
