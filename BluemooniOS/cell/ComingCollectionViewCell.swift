//
//  ComingCollectionViewCell.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit

class ComingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var progress: CustomProgressView!
    @IBOutlet weak var like: UIButton!
    @IBOutlet weak var timerText: UILabel!
    @IBOutlet weak var salesPercent: UILabel!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var originPrice: UILabel!
    @IBOutlet weak var discountRate: UILabel!
    @IBOutlet weak var price: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 0.3
//        self.layer.cornerRadius = 15
    }
    
    func setOriginPrice(text: String!) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        originPrice.attributedText = attributeString
        originPrice.isHidden = false;
    }
    
    func setEndTime(time: String!) {
    
    }
    
    var timer: Timer?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        timer?.invalidate()
    }
}
