//
//  HomeSaleCollectionViewCell.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 23/04/2019.
//

import UIKit

class HomeSaleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var likeImg: UIButton!
    @IBOutlet weak var progress: CustomProgressView!
    @IBOutlet weak var timerText: UILabel!
    @IBOutlet weak var salesPercent: UILabel!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var originPrice: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var discountRate: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 0.3
//        self.layer.cornerRadius = 15
    }
    
    var timer: Timer?
    
    func setOriginPrice(text: String!) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        originPrice.attributedText = attributeString
        originPrice.isHidden = false;
    }
    
    func setEndTime(time: String!) {
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        timer?.invalidate()
    }
}
