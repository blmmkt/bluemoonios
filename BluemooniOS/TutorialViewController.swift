//
//  TutorialViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 05/05/2019.
//

import UIKit
import ImageSlideshow

class TutorialViewController: UIViewController {

    @IBOutlet weak var tutorialSlide: ImageSlideshow!
    @IBOutlet weak var tutorialLabel: UILabel!
    @IBOutlet weak var tutorialEndButton: CustomButton!
    
    weak var splashDelegate: TutorialDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setWidgets()
        
        setImageSlideshowPagechanger()
    }
    
    func setWidgets() {
        tutorialSlide.setImageInputs([ImageSource(image: UIImage(named: "tutorial_1")!), ImageSource(image: UIImage(named: "tutorial_2")!), ImageSource(image: UIImage(named: "tutorial_3")!), ImageSource(image: UIImage(named: "tutorial_4")!), ImageSource(image: UIImage(named: "tutorial_5")!),])
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .lightGray
        pageControl.pageIndicatorTintColor = .black
        tutorialSlide.pageIndicator = pageControl
        
        tutorialSlide.circular = false
        
        tutorialLabel.text = NSLocalizedString("tutorial1", comment: "")
        
        tutorialEndButton.setInit()
        tutorialEndButton.setTitle(NSLocalizedString("tutorial_end", comment: ""), for: .normal)
    }
    
    func setImageSlideshowPagechanger() {
        tutorialSlide.currentPageChanged = { page in
            switch page {
            case 1:
                self.tutorialLabel.text = NSLocalizedString("tutorial2", comment: "")
                break
            case 2:
                self.tutorialLabel.text = NSLocalizedString("tutorial3", comment: "")
                break
            case 3:
                self.tutorialLabel.text = NSLocalizedString("tutorial4", comment: "")
                break
            case 4:
                self.tutorialLabel.text = NSLocalizedString("tutorial5", comment: "")
                self.tutorialEndButton.isHidden = false
                break
            default:
                break
            }
        }
    }
    
    @IBAction func tutorialEndAction(_ sender: UIButton) {
        Constants.USER_DEFAULT.setValue(true, forKey: "tutorial")
        dismiss(animated: true) {
            if self.splashDelegate != nil {
                self.splashDelegate?.checkTutorial()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
