//
//  OrderListViewController.swift
//  BluemooniOS
//
//  Created by Soso on 05/05/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class OrderListViewController: UIViewController {
    
    let orderTableViewCellId = "orderTableViewCellId"
    let orderTableViewHeaderId = "orderTableViewHeaderId"
    let orderTableViewFooterId = "orderTableViewFooterId"
    
    @IBOutlet weak var tableView: UITableView!
    
    var imagPath: String = ""
    var orders: [ResOrder.Order] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupData()
    }
    
    func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: orderTableViewCellId)
        tableView.register(UINib(nibName: "OrderTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: orderTableViewHeaderId)
        tableView.register(UINib(nibName: "OrderTableViewFooter", bundle: nil), forHeaderFooterViewReuseIdentifier: orderTableViewFooterId)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 2))
    }
    
    func setupData() {
        Alamofire.request(
            Constants.BASE_URL + "Buy_List_Token.jsp",
            method: .post,
            parameters: [
                "token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? ""
            ],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: { (response) in
                print(response)
            })
            .responseObject(completionHandler: { (response: DataResponse<ResOrder>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        print(result)
                        
                        self.imagPath = result.imgPath
                        self.orders = result.check_out_list
                        self.tableView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
            })
    }
    
}

extension OrderListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders[section].list_item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: orderTableViewCellId) as! OrderTableViewCell
        cell.tag = indexPath.row
        let element = orders[indexPath.section].list_item[indexPath.row]
        cell.element = element
        if let string = (imagPath + element.thumbnail).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: string) {
            cell.elementImageView.kf.setImage(with: url)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: orderTableViewHeaderId) as! OrderTableViewHeader
        view.order = orders[section]
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        //        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: orderTableViewFooderId) as! OrderTableViewFooter
        let view = UIView()
        view.backgroundColor = .white
        let view2 = UIView()
        view2.backgroundColor = .black
        view.addSubview(view2)
        view2.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().offset(2)
            make.right.equalToSuperview().offset(-2)
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (orders[indexPath.row].invoice_num == "Checking by seller" ||
            orders[indexPath.row].invoice_num == "ready") {
            Alamofire.request(
                Constants.BASE_URL + "Buy_Fix_Cancle.jsp",
                method: .post,
                parameters: ["token": Constants.USER_DEFAULT.string(forKey: "id") ?? "",
                             "order_id": String(self.orders[indexPath.row].order_id)],
                encoding: URLEncoding.default,
                headers: nil
                )
                .validate(statusCode: 200..<300)
                .responseObject { (response: DataResponse<ResFind>) in
                    if let result = response.result.value {
                        self.orders.remove(at: indexPath.row)
                        
                        self.tableView.reloadData()
                    }
            }
        }
    }
}
