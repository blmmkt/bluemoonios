//
//  OrderTableViewCell.swift
//  BluemooniOS
//
//  Created by Soso on 05/05/2019.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    var element: ResOrder.Order.Element? {
        didSet {
            guard let element = element else { return }
            manufacturerLabel.text = element.manufacturer
            nameLabel.text = element.name
            detailLabel.text = element.currentDetail
            priceLabel.text = element.currentPrice
        }
    }
    
    @IBOutlet weak var elementImageView: UIImageView!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
}
