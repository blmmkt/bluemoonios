//
//  OrderTableViewHeader.swift
//  BluemooniOS
//
//  Created by Soso on 05/05/2019.
//

import UIKit

class OrderTableViewHeader: UITableViewHeaderFooterView {
    
    var order: ResOrder.Order? {
        didSet {
            guard let order = order else { return }
            invoiceContentLabel.text = order.invoice_num
            orderContentLabel.text = order.order_id
            priceLabel.text = order.currentTotal
        }
    }
    
    @IBOutlet weak var invoiceLabel: UILabel!
    @IBOutlet weak var invoiceContentLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var orderContentLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        invoiceLabel.text = "invoice_title".localized()
        orderLabel.text = "order_id_title".localized()
    }
    
}
