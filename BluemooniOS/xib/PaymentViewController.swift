//
//  PaymentViewController.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import UIKit
import ImageSlideshow
import Alamofire
import Firebase
import Toaster

class PaymentViewController: UIViewController, EditAddressDelegate {
    var path: String
    var items: [Item]
    var availablePoint: Int = 0 {
        didSet {
            pointTitle3Label.text = "check_out_point_title3".localized() + " \(availablePoint)"
        }
    }
    var currentPoint: Float = 0 {
        didSet {
            subTotalAmountLabel.text = "\(subTotal - currentPoint) " + UtilMethod.getLocalUnit()
            grandTotal = subTotal - currentPoint + deliveryTotal
        }
    }
    var subTotal: Float = 0 {
        didSet {
            subTotalAmountLabel.text = "\(subTotal - currentPoint) " + UtilMethod.getLocalUnit()
            grandTotal = subTotal - currentPoint + deliveryTotal
        }
    }
    var deliveryTotal: Float = 0 {
        didSet {
            deliveryTotalAmountLabel.text = deliveryTotal == 0 ? "Free" : "\(UtilMethod.getLocalDeliveryFee()) " + UtilMethod.getLocalUnit()
            grandTotal = subTotal - currentPoint + deliveryTotal
        }
    }
    var grandTotal: Float = 0 {
        didSet {
            grandTotalAmountLabel.text = "\(grandTotal) " + UtilMethod.getLocalUnit()
        }
    }
    
    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var imageSlideShowView: ImageSlideshow!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var deliveryAddressLabel: UILabel!
    @IBOutlet weak var deliveryAddressContentLabel: UILabel!
    @IBOutlet weak var editAddress: UIButton!
    @IBOutlet weak var billingAddressLabel: UILabel!
    @IBOutlet weak var billingAddressButton: UIButton!
    @IBOutlet weak var billingAddressContentLabel: UILabel!
    @IBOutlet weak var editBill: UIButton!
    @IBOutlet weak var deliveryOptionLabel: UILabel!
    @IBOutlet weak var emsDeliveryLabel: UILabel!
    @IBOutlet weak var emsDeliveryButton: UIButton!
    @IBOutlet weak var freeDeliveryLabel: UILabel!
    @IBOutlet weak var freeDeliveryButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pointTitle1Label: UILabel!
    @IBOutlet weak var pointTitle2Label: UILabel!
    @IBOutlet weak var pointTitle3Label: UILabel!
    @IBOutlet weak var pointTextField: UITextField!
    @IBOutlet weak var optionBank: UIButton!
    @IBOutlet weak var optionCredit: UIButton!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var subTotalAmountLabel: UILabel!
    @IBOutlet weak var deliveryTotalLabel: UILabel!
    @IBOutlet weak var deliveryTotalAmountLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var grandTotalAmountLabel: UILabel!
    
    enum PAY_OPTION {
        case BANK, CREDIT
    }
    
    var option = PAY_OPTION.BANK
    
    var name: String!
    var phone: String!
    var city: String!
    var country: String!
    var firstName: String!
    var lastName: String!
    var phoneNum: String!
    var zipCode: String!
    var street1: String!
    var datas = [String](repeating: "", count: 9)
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            billingAddressContentLabel.text = deliveryAddressContentLabel.text
        }
    }
    
    @IBAction func deliveryButtonAction(_ sender: UIButton) {
        sender.isSelected = true
        if sender == emsDeliveryButton {
            freeDeliveryButton.isSelected = false
        } else {
            emsDeliveryButton.isSelected = false
        }
        deliveryTotal = freeDeliveryButton.isSelected ? 0 : UtilMethod.getLocalDeliveryFee()
    }
    
    @IBAction func optionButtonAction(_ sender: UIButton) {
        sender.isSelected = true
        if sender == optionBank {
            optionCredit.isSelected = false
            option = PAY_OPTION.BANK
        } else {
            optionBank.isSelected = false
            option = PAY_OPTION.CREDIT
        }
        
    }
    
    @IBAction func orderButtonAction(_ sender: UIButton) {
        if self.datas.count == 0 {
            let alert = UIAlertController.init(title: "Empty address", message: "Address is empty. Please fill address.", preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true)
        } else {
            if (option == PAY_OPTION.BANK) {
                if self.items.count == 1 {
                    Alamofire.request(
                        Constants.BASE_URL + "without_bankbook.jsp",
                        method: .post,
                        parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                                     "detail_id": items.first!.itemId,
                                     "amount": items.first!.quantity,
                                     "currency": UtilMethod.getLocalCurrecyType(),
                                     "usePoint": Int(pointTextField.text!) ?? 0,
                                     "surcharge": Int(deliveryTotal),
                                     "name": datas[0],
                                     "phone": datas[1],
                                     "shipTo_city": datas[2],
                                     "shipTo_country": "VN",
                                     "shipTo_firstName": datas[4],
                                     "shipTo_lastName": datas[5],
                                     "shipTo_phoneNumber": datas[6],
                                     "shipTo_postalCode": datas[7],
                                     "shipTo_street1": datas[8]],
                        encoding: URLEncoding.default,
                        headers: nil
                        )
                        .validate(statusCode: 200..<300)
                        .responseObject {(response: DataResponse<ResFind>) in
                            if let result = response.result.value {
                                if(result.error_msg != "ok") {
                                    
                                    Toast(text: "error occurred. Contact to bluemoonmkt.it@gmail.com", delay: 0, duration: 2.0).show()
                                    return
                                } else {
                                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bankDoneStory") as! BankDoneViewController
                                    vc.total = self.grandTotalAmountLabel.text!
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                    }
                } else {
                    Alamofire.request(
                        Constants.BASE_URL + "without_bankbook.jsp",
                        method: .post,
                        parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                                     "cart_ids": items.map({ $0.seq }).joined(separator: "|"),
                                     "currency": UtilMethod.getLocalCurrecyType(),
                                     "usePoint": Int(pointTextField.text!) ?? 0,
                                     "surcharge": Int(deliveryTotal),
                                     "name": datas[0],
                                     "phone": datas[1],
                                     "shipTo_city": datas[2],
                                     "shipTo_country": "VN",
                                     "shipTo_firstName": datas[4],
                                     "shipTo_lastName": datas[5],
                                     "shipTo_phoneNumber": datas[6],
                                     "shipTo_postalCode": datas[7],
                                     "shipTo_street1": datas[8]],
                        encoding: URLEncoding.default,
                        headers: nil
                        )
                        .validate(statusCode: 200..<300)
                        .responseObject {(response: DataResponse<ResFind>) in
                            if let result = response.result.value {
                                if(result.error_msg != "ok") {
                                    Toast(text: "error occurred. Contact to bluemoonmkt.it@gmail.com", delay: 0, duration: 5.0).show()
                                    return
                                } else {
                                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bankDoneStory") as! BankDoneViewController
                                    vc.total = self.grandTotalAmountLabel.text!
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                    }
                }
            } else {
                let webVC = WebViewController(items: items, usePoint: Int(pointTextField.text!) ?? 0, fee: Int(deliveryTotal), datas: self.datas)
                navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
    
    init(path: String, items: [Item]) {
        self.path = path
        self.items = items
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupData()
        
        Analytics.logEvent("Buy", parameters: [
            "start": "buy" as NSObject
            ])
    }
    
    func setupView() {
        placeOrderButton.setTitle("place_order_title".localized(), for: .normal)
        orderLabel.text = "my_order_title".localized()
        addressLabel.text = "my_address_title".localized()
        deliveryAddressLabel.text = "delivery_address_title".localized()
        billingAddressLabel.text = "billing_address_title".localized()
        billingAddressButton.setTitle("billing_address_same".localized(), for: .normal)
        deliveryOptionLabel.text = "my_delivery_options".localized()
        emsDeliveryLabel.text = "check_out_delivery_ems".localized()
        emsDeliveryButton.setTitle("check_out_ems_title".localized(), for: .normal)
        freeDeliveryLabel.text = "check_out_delivery_free".localized()
        freeDeliveryButton.setTitle("check_out_free_title".localized(), for: .normal)
        pointTitle1Label.text = "check_out_point_title".localized()
        pointTitle2Label.text = "check_out_point_title2".localized()
        pointTitle3Label.text = "check_out_point_title3".localized()
        pointTextField.placeholder = "check_out_point_maximum".localized()
        subTotalLabel.text = "check_out_sub_total".localized()
        deliveryTotalLabel.text = "check_out_delivery_total".localized()
        grandTotalLabel.text = "check_out_grand_total".localized()
        
        let inputs = items.compactMap({ (item) -> KingfisherSource? in
            if let string = (path + item.thumbId).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let url = URL(string: string) {
                return KingfisherSource(url: url)
            }
            return nil
        })
        imageSlideShowView.setImageInputs(inputs)
        imageSlideShowView.pageControl.currentPageIndicatorTintColor = .lightGray
        imageSlideShowView.pageControl.pageIndicatorTintColor = .black
        
        emailTextField.leftViewMode = .always
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        
        pointTextField.delegate = self
        pointTextField.leftViewMode = .always
        pointTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        pointTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        subTotal = items.map({ $0.price * Float($0.quantity) }).reduce(0, +)
        currentPoint = 0
        deliveryTotal = 0
    }
    
    func setupData() {
        Alamofire.request(
            Constants.BASE_URL + "User_Info_Ok.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? ""],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResUserInfo>) in
                if let result = response.result.value {
                    if(result.error_msg == "") {
                        var info = ""
                        if result.first_name == "" {
                            info = """
                            \("Guest")
                            \("")
                            \("Please edit delivery address")
                            \("")
                            """
                            
                            self.name = "Guest"
                        } else {
                            info = """
                            \(result.first_name!) \(result.last_name!)
                            \(result.address1!)
                            \(result.address2!)
                            \(result.city!) \(result.province!) \(result.zip_code!)
                            \(result.country!)
                            \(result.phone_num!)
                            """
                            
                            self.name = result.first_name! + " " + result.last_name!
                            self.phone = result.phone_num
                            self.city = result.city
                            self.country = result.province
                            self.firstName = result.first_name
                            self.lastName = result.last_name
                            self.phoneNum = result.phone_num
                            self.zipCode = result.zip_code
                            self.street1 = result.address1 + " " + result.address2
                            
                            self.emailTextField.text = result.email
                            
                            self.datas[0] = self.name
                            self.datas[1] = self.phone
                            self.datas[2] = self.city
                            self.datas[3] = self.country
                            self.datas[4] = self.firstName
                            self.datas[5] = self.lastName
                            self.datas[6] = self.phoneNum
                            self.datas[7] = self.zipCode
                            self.datas[8] = self.street1
                        }
                        
                        let attributedString = NSMutableAttributedString(string: info)
                        let paragraphStyle = NSMutableParagraphStyle()
                        paragraphStyle.lineSpacing = 5
                        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: NSMakeRange(0, attributedString.length))
                        
                        self.deliveryAddressContentLabel.attributedText = attributedString
                        self.billingAddressContentLabel.attributedText = attributedString
                        self.availablePoint = result.point
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    @IBAction func editShipAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "editAddressStory") as! EditAddressViewController
        vc.editDelegate = self
        vc.what = "ship"
        
        self.present(vc, animated: true)
    }
    
    @IBAction func editBillAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "editAddressStory") as! EditAddressViewController
        vc.editDelegate = self
        vc.what = "bill"
        
        self.present(vc, animated: true)
    }
    
    func EditShip(name: String, add1: String, add2: String, country: String, province: String, city: String, zip: String, phone: String) {
        self.billingAddressButton.setImage(UIImage(named: "checkbox-unchecked-black"), for: .disabled)
        
        let info = """
        \(name)
        \(add1)
        \(add2)
        \(city) \(province) \(zip)
        \(country)
        \(phone)
        """
        
        self.name = name
        self.phone = phone
        self.city = city
        self.country = province
        self.firstName = String(name.split(separator: " ")[0])
        self.lastName = String(name.split(separator: " ")[1])
        self.phoneNum = phone
        self.zipCode = zip
        self.street1 = add1 + " " + add2
        
        self.datas[0] = self.name
        self.datas[1] = self.phone
        self.datas[2] = self.city
        self.datas[3] = self.country
        self.datas[4] = self.firstName
        self.datas[5] = self.lastName
        self.datas[6] = self.phoneNum
        self.datas[7] = self.zipCode
        self.datas[8] = self.street1
        
        let attributedString = NSMutableAttributedString(string: info)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        self.deliveryAddressContentLabel.attributedText = attributedString
    }
    
    func EditBill(name: String, add1: String, add2: String, country: String, province: String, city: String, zip: String, phone: String) {
        self.billingAddressButton.setImage(UIImage(named: "checkbox-unchecked-black"), for: .disabled)
        
        let info = """
        \(name)
        \(add1)
        \(add2)
        \(city) \(province) \(zip)
        \(country)
        \(phone)
        """
        
        self.city = city
        self.country = province
        self.zipCode = zip
        self.street1 = add1 + " " + add2
        
        let attributedString = NSMutableAttributedString(string: info)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        self.billingAddressContentLabel.attributedText = attributedString
    }
}

extension PaymentViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if let point = updatedText.isEmpty ? 0 : Int(updatedText) {
                if point > availablePoint {
                    UIAlertController.show(self, title: "Notice", message: "check_out_point_error2".localized())
                    return false
                } else if point > 25000 {
                    UIAlertController.show(self, title: "Notice", message: "check_out_point_error".localized())
                    return false
                } else {
                    return true
                }
            }
        }
        
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, let point = text.isEmpty ? 0 : Int(text) {
            currentPoint = Float(point) * UtilMethod.getLocalPointRatio()
        }
    }
}
