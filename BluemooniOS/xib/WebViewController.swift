//
//  WebViewController.swift
//  BluemooniOS
//
//  Created by Soso on 05/05/2019.
//

import UIKit
import WebKit
import WebViewJavascriptBridge

class WebViewController: UIViewController {
    
    var items: [Item]
    var point: Int
    var fee: Int
    var datas: [String]
    
    init(items: [Item], usePoint: Int, fee: Int, datas: [String]) {
        self.point = usePoint
        self.items = items
        self.fee = fee
        self.datas = datas
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var webView: UIWebView!
    
    var bridge: WebViewJavascriptBridge?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bridge = WebViewJavascriptBridge(forWebView: webView)
        bridge?.registerHandler("onEventClose", handler: { (parameters, callback) in
            if let json = parameters as? [String: String] {
                if json["status"] == "success" {
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                    self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                }
            }
        })
        
        let url = Constants.BASE_URL + "eximbay_request.jsp"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        var postData = "token=" + (Constants.USER_DEFAULT.string(forKey: "tkn") ?? "")
        
        if items.count == 1 {
            guard let item = items.first else { return }
            postData += "&detail_id=" + item.itemId
                + "&amount=\(item.quantity)"
                + "&currency=" + UtilMethod.getLocalCurrecyType()
                + "&usePoint=\(point)"
                + "&surcharge=\(fee)"
                + "&name=\(datas[0])"
                + "&phone=\(datas[1])"
                + "&shipTo_city=\(datas[2])"
                + "&shipTo_country=VN"
                + "&shipTo_firstName=\(datas[4])"
                + "&shipTo_lastName=\(datas[5])"
                + "&shipTo_phoneNumber=\(datas[6])"
                + "&shipTo_postalCode=\(datas[7])"
                + "&shipTo_street1=\(datas[8])"
        } else {
            postData += "&cart_ids=" + items.map({ $0.seq }).joined(separator: "|")
                + "&currency=" + UtilMethod.getLocalCurrecyType()
                + "&usePoint=\(point)"
                + "&surcharge=\(fee)"
                + "&name=\(datas[0])"
                + "&phone=\(datas[1])"
                + "&shipTo_city=\(datas[2])"
                + "&shipTo_country=VN"
                + "&shipTo_firstName=\(datas[4])"
                + "&shipTo_lastName=\(datas[5])"
                + "&shipTo_phoneNumber=\(datas[6])"
                + "&shipTo_postalCode=\(datas[7])"
                + "&shipTo_street1=\(datas[8])"
        }
        
        print(postData)
        
        request.httpBody = postData.data(using: .utf8)
        webView.loadRequest(request)
    }

}
