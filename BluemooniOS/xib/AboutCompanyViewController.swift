//
//  AboutCompanyViewController.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import UIKit

class AboutCompanyViewController: UIViewController {
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var doneButton: CustomButton!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .overCurrentContext
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        informationLabel.text = NSLocalizedString("office_info", comment: "")
        doneButton.setInit()
        doneButton.setTitle(NSLocalizedString("complete_positive", comment: ""), for: .normal)
        doneButton.addTarget(self, action: #selector(handleDone), for: .touchUpInside)
    }
    
    @objc func handleDone() {
        dismiss(animated: true, completion: nil)
    }

}
