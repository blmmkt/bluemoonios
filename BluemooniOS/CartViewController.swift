//
//  CartViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Firebase

class CartViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CartDelegate, LoginDelegate {
    
    @IBOutlet weak var cartCollectionView: UICollectionView!
    @IBOutlet weak var buyButton: CustomImageButton!
    
    var cartDataList: Array<ResCart.Cart>! = []
    var items: [Item]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0 {
            buyButton.snp.updateConstraints{(make) in
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-50)
            }
        } else {
            buyButton.snp.updateConstraints{(make) in
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(-50)
            }
        }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
        setWidgets()
//        getCartData()
        
        Analytics.logEvent("Cart", parameters: [
            "cart": "see" as NSObject
            ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getCartData()
    }
    
    func setWidgets() {
        cartCollectionView.delegate = self
        cartCollectionView.dataSource = self
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        
        buyButton.setInit()
        buyButton.customImageView.image = #imageLiteral(resourceName: "icon_buy_white")
        buyButton.customTitleLabel.text = "Buy"
        buyButton.backgroundColor = .black
        buyButton.addTarget(self, action: #selector(handleBuy(_:)), for: .touchUpInside)
    }
    
    @objc func handleBuy(_ sender: UIButton) {
        items = cartDataList.filter({ $0.state == "s" }).map({ Item(seq: $0.seq, itemId: $0.id, thumbId: String($0.thumbnails.split(separator: "|").first!), price: $0.currentPrice, quantity: $0.amount) })
        
        if items.count > 0 {
            if (Constants.USER_DEFAULT.string(forKey: "id")?.contains("guest"))! {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "signInStory") as! SignInViewController
                vc.delegate = self
                self.present(vc, animated: true)
            } else {
                loginSuccess()
            }
        }
    }
    
    func loginSuccess() {
        let paymentVC = PaymentViewController(path: cartDataList.first!.imgPath, items: items)
        navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cartDataList.count == 0 ? 1 : cartDataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cartCell", for: indexPath) as! CartCollectionViewCell
        
        if cartDataList.count == 0 {
            cell.imageView.image = UIImage(named: "icon_empty")
            cell.brand.text = NSLocalizedString("no_item", comment: "")
            cell.product.isHidden = true
            cell.options.isHidden = true
            cell.price.isHidden = true
            cell.editBtn.isHidden = true
            cell.deleteBtn.isHidden = true
            
            return cell
        } else {
            cell.product.isHidden = false
            cell.options.isHidden = false
            cell.price.isHidden = false
            cell.editBtn.isHidden = false
            cell.deleteBtn.isHidden = false
            
            let comingData = self.cartDataList[indexPath.row]
            
            cell.delegate = self
            cell.tag = indexPath.row
            
            let oriUrl = comingData.imgPath + comingData.thumbnails.split(separator: "|")[0]
            let kor = oriUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let urlKor = URL(string: kor)
            
            cell.imageView.kf.setImage(with: urlKor)
            
            cell.brand.text = comingData.manufacturer
            cell.product.text = comingData.name
            
            let size = UtilMethod.changeNumToSize(num: String(comingData.id.split(separator: "_")[2])) + " / "
            let color = comingData.id.split(separator: "_")[1] + " / "
            
            let amount = String(comingData.amount) + " ea"
            
            let option = size + color + amount
            
            cell.options.text = option
            
            if comingData.sold_out == false {
                cell.price.text = UtilMethod.getForattedPrice(price: UtilMethod.getLocalPrice(priceString: comingData.price) * Float(comingData.amount)) + " " + UtilMethod.getLocalUnit()
            } else {
                // TODO : 솔드아웃 넣을것
    //            cell.price.text =
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 8
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize, height: 196)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "cartOffice", for: indexPath)
        
        return footer
    }

    func getCartData() {
        Alamofire.request(
            Constants.BASE_URL + "Cart_getList_Token.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "page": 0],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResCart>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.cartDataList.removeAll()
                        
                        for item in result.cart_list {
                            if item.state != "e" {
                                self.cartDataList.append(item)
                            }
                        }
                        
                        self.cartCollectionView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func editPressed(_ cell: CartCollectionViewCell) {
        Analytics.logEvent("Cart", parameters: [
            "cart": "edit" as NSObject
            ])
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "detailComingSoonViewController") as! DetailComingSoonViewController
        
        vc.isEdit = true
        let selectedItem = cartDataList[cell.tag]
        vc.editId = selectedItem.id
        let product = CartToTease(mId: String(selectedItem.id.split(separator: "_")[0]), mName: selectedItem.name, mManufacturer: selectedItem.manufacturer, mPrice: selectedItem.price, mImagePath: selectedItem.imgPath, mthumbnails: selectedItem.thumbnails, mDiscount: selectedItem.discount, mDiscountRate: selectedItem.discount_rate, mSales: selectedItem.sales, mSaleEnd: selectedItem.sale_end, mSoldOut: selectedItem.sold_out, mMyLike: selectedItem.my_like, mLike: selectedItem.like, mMinimum: selectedItem.minimum, mMaximum: selectedItem.maximum, mDescriptionText1: selectedItem.description_text1, mDescriptionText2: selectedItem.description_text2, mDescriptionImg: selectedItem.description_img)
        vc.product = product
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func deletePressed(_ cell: CartCollectionViewCell) {
        let alert = UIAlertController.init(title: NSLocalizedString("cart_delete_title", comment: ""), message: "", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: NSLocalizedString("sign_up_cancel", comment: ""), style: .default, handler: nil)
        let deleteAction = UIAlertAction.init(title: NSLocalizedString("cart_delete", comment: ""), style: .destructive) { (UIAlertAction) in
            DispatchQueue.main.async {
                self.deleteItem(cell)
                
                Analytics.logEvent("Cart", parameters: [
                    "cart": "delete" as NSObject
                    ])
            }
        }
        
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        self.present(alert, animated: true)
    }
    
    func deleteItem(_ cell: CartCollectionViewCell) {
        Alamofire.request(
            Constants.BASE_URL + "Product_DeleteCart.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(self.cartDataList[cell.tag].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if (result.error_msg == "ok") {
                        self.cartDataList.remove(at: cell.tag)
                        self.cartCollectionView.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
}
