//
//  DetailComingSoonViewController.swift
//  BluemooniOS
//
//  Created by Soso on 01/05/2019.
//

import UIKit
import ImageSlideshow
import SnapKit
import Kingfisher
import Alamofire
import Firebase
import WebKit

class DetailComingSoonViewController: UIViewController, WKNavigationDelegate {
    
    var product: Product!
    var customAddCartView: CustomAddCartView?
    
    @IBOutlet weak var imageSlideShowView: ImageSlideshow!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var originalPriceLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var deliveryDateLabel: UILabel!
    @IBOutlet weak var deliveryConstraint: NSLayoutConstraint!
    @IBOutlet weak var description1Title: UILabel!
    @IBOutlet weak var description1Label: UILabel!
    @IBOutlet weak var description2Title: UILabel!
    @IBOutlet weak var description2Label: UILabel!
    @IBOutlet weak var detailStackView: UIStackView!
    @IBOutlet weak var detailImageTitle: UILabel!
    @IBOutlet weak var detailButtonView: UIView!
    @IBOutlet weak var detailImageButton: UIButton!
    @IBOutlet weak var detailImageView: WKWebView!
    @IBOutlet weak var progressView: CustomProgressView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var refundLabel: UILabel!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var webViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackInnerHeight: NSLayoutConstraint!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    
    var isEdit: Bool = false
    var editId: String!
    var size: String = ""
    var color: String = ""
    var quantity: String = ""
    
    @IBAction func detailButtonAction(_ sender: UIButton) {
        if !(product is ResCow.Cow) {
            if product is ResSales.Sale {
                Analytics.logEvent("DetailImage", parameters: [
                    "detailImage": "sale" as NSObject
                    ])
            } else {
                Analytics.logEvent("DetailImage", parameters: [
                    "detailImage": "comingsoon" as NSObject
                    ])
            }
        }
        
        if let url = product.description_img,
            let path = product.imgPath,
            let string = (path + url).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let imageUrl = URL(string: string) {
            detailImageView.navigationDelegate = self
            detailImageView.load(URLRequest(url: imageUrl))
            detailImageView.scrollView.isScrollEnabled = true
            self.detailImageButton.isHidden = true
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (height, error) in
            let ratio = height as! CGFloat / CGFloat(1080)
            self.stackHeight?.constant = (self.detailImageView.frame.width * ratio + CGFloat(48))
            self.stackInnerHeight?.constant = (self.detailImageView.frame.width * ratio + CGFloat(48))
            self.webViewHeight?.constant = (self.detailImageView.frame.width * ratio)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupData()
    }
    
    func setupView() {
        view.backgroundColor = .white
        
        if !(product is ResCow.Cow) {
            let customAddCartView = CustomAddCartView(product: product)
            self.customAddCartView = customAddCartView
            customAddCartView.delegate = self
            view.addSubview(customAddCartView)
            customAddCartView.snp.makeConstraints { (make) in
                make.left.right.equalToSuperview()
                make.top.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-CustomAddCartView.tabBarHeight)
            }
//            scrollViewBottomConstraint.constant = CustomAddCartView.tabBarHeight
            if !(product is ResSales.Sale) {
                deliveryDateLabel.isHidden = true
                deliveryConstraint.constant = 0 - deliveryDateLabel.frame.height
            } else {
                deliveryDateLabel.text = NSLocalizedString("delivery_date", comment: "")
            }
        } else {
            deliveryDateLabel.isHidden = true
            deliveryConstraint.constant = 0 - deliveryDateLabel.frame.height
        }
        
        let thumbnails = product.thumbnails.split(separator: "|")
        let inputs = thumbnails.compactMap({ (thumbnail) -> KingfisherSource? in
            if let string = (product.imgPath + thumbnail).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let url = URL(string: string) {
                return KingfisherSource(url: url)
            }
            return nil
        })
        imageSlideShowView.setImageInputs(inputs)
        imageSlideShowView.pageControl.currentPageIndicatorTintColor = .white
        imageSlideShowView.pageControl.pageIndicatorTintColor = .black
        
        manufacturerLabel.text = product.manufacturer
        nameLabel.text = product.name
//        originalPriceLabel.isHidden = !(product is ResSales.Sale)
        originalPriceLabel.isHidden = !(product.discount == "Y")
        originalPriceLabel.attributedText = product.originalPriceText
        currentPriceLabel.attributedText = product.currentPriceText
        
        progressView.setSecondaryProgress(progress: Float(product.minimum / product.maximum))
        progressView.setProgress(Float(product.sales / product.maximum), animated: true)
        discountLabel.text = String(Int(Float(product.sales / product.minimum) * 100)) + "%"


        description1Title.text = NSLocalizedString("product_detail", comment: "")
        description1Label.text = product.description_text1
        description2Title.text = NSLocalizedString("material_detail", comment: "")
        description2Label.text = product.description_text2
        
        detailImageTitle.text = NSLocalizedString("more_detail", comment: "")
        detailImageButton.setTitle(NSLocalizedString("open", comment: ""), for: .normal)

        if let path = Bundle.main.path(forResource: "refund_exchange_en", ofType: "txt") {
            do {
                let text = try String(contentsOfFile: path)
                refundLabel.text = text
            } catch {
                print("Couldn't load: \(path)")
            }
        }

        informationLabel.text = NSLocalizedString("office_info", comment: "")
        
        var date: Date?
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let sale = self.product as? ResSales.Sale {
            date = dateFormatter.date(from: sale.sale_end)
        } else if let tease = self.product as? ResTease.Tease {
            date = dateFormatter.date(from: tease.sale_start)
        }
        
        if let date = date {
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                self.timerLabel.text = date.countDownFormat
                if Date() > date {
                    timer.invalidate()
                }
            })
            timer?.fire()
            RunLoop.current.add(timer!, forMode: .common)
        }
        
    }
    
    var timer: Timer?
    
    func setupData() {
        Alamofire.request(
            Constants.BASE_URL + "Product_SizeColor.jsp",
            method: .post,
            parameters: [
                "token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                "item_id": product.id!
            ],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject(completionHandler: { (response: DataResponse<ResSizeColor>) in
                if let result = response.result.value {
                    if(result.error_msg == "id error") {
                        let idAlert = UIAlertController.init(title: NSLocalizedString("id_error_title", comment: ""), message: NSLocalizedString("id_fail", comment: ""), preferredStyle: .alert)
                        idAlert.addAction(self.dismissAlert)
                        self.present(idAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "pw error") {
                        let pwAlert = UIAlertController.init(title: NSLocalizedString("pw_error_title", comment: ""), message: NSLocalizedString("pw_fail", comment: ""), preferredStyle: .alert)
                        pwAlert.addAction(self.dismissAlert)
                        self.present(pwAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "ok") {
                        let sizes = result.size.split(separator: "|").map({ UtilMethod.changeNumToSize(num: String($0)) })
                        let colors = result.color.split(separator: "|").map({ String($0) })
                        self.customAddCartView?.setupData(sizes: sizes, colors: colors)
                    } else {
                        print(result.error_msg)
                    }
                }
            })
    }
    
    func sendToCart(size: String, color: String, quantity: String) {
        if isEdit {
            Alamofire.request(
                Constants.BASE_URL + "Product_DeleteCart.jsp",
                method: .post,
                parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                             "item_id": String(editId)],
                encoding: URLEncoding.default,
                headers: nil
                )
                .validate(statusCode: 200..<300)
                .responseObject {(response: DataResponse<ResFind>) in
                    if let result = response.result.value {
                        if (result.error_msg == "ok") {
                            self.addToCart(size, color: color, quantity: quantity)
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            print(result.error_msg)
                        }
                    }
            }
        } else {
            self.addToCart(size, color: color, quantity: quantity)
        }
    }
    
    func addToCart(_ size: String, color: String, quantity: String) {
        Alamofire.request(
            Constants.BASE_URL + "Product_SendCart.jsp",
            method: .post,
            parameters: [
                "token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                "item_id": "\(product.id!)_\(color)_\(size)",
                "quantity": quantity
            ],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: { (response) in
                print(response)
            })
            .responseObject(completionHandler: { (response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "id error") {
                        let idAlert = UIAlertController.init(title: NSLocalizedString("id_error_title", comment: ""), message: NSLocalizedString("id_fail", comment: ""), preferredStyle: .alert)
                        idAlert.addAction(self.dismissAlert)
                        self.present(idAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "pw error") {
                        let pwAlert = UIAlertController.init(title: NSLocalizedString("pw_error_title", comment: ""), message: NSLocalizedString("pw_fail", comment: ""), preferredStyle: .alert)
                        pwAlert.addAction(self.dismissAlert)
                        self.present(pwAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "ok") {
                        print(result)
                    } else {
                        print(result.error_msg)
                    }
                }
            })
    }
    
    let dismissAlert = UIAlertAction(title: NSLocalizedString("complete_positive", comment: ""), style: .cancel, handler: nil)

    func handleToggle(state: Bool) {
        UIView.animate(withDuration: 0.3) {
            if let customAddCartView = self.customAddCartView {
                if UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0 {
                    customAddCartView.backgroundColor = state ? .white : .black
//                        Constants.COLOR_BASIC
                    customAddCartView.sizePickerView.backgroundColor = state ? .white : .black
//                        Constants.COLOR_BASIC
                    customAddCartView.sizePickerView.borderColor = state ? .black : .black
//                        Constants.COLOR_BASIC
                }
                
                let height: CGFloat = state ?
                    -customAddCartView.bounds.height :
                    -CustomAddCartView.tabBarHeight
                customAddCartView.snp.updateConstraints { (make) in
                    make.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).offset(height)
                }
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var flag = 0
}

extension DetailComingSoonViewController: CustomAddCartViewProtocol, LoginDelegate {
    
    func didPressLike(state: Bool) {
        
    }
    
    func didPressToggle(state: Bool, sender: UIButton) {
        flag = sender == customAddCartView?.cartButton ? 0 : 1
        handleToggle(state: state)
    }
    
    func didPressDone(size: String, color: String, quantity: String) {
        handleToggle(state: false)
        if flag == 0 {
            sendToCart(size: size, color: color, quantity: quantity)
        } else {
            self.size = size
            self.color = color
            self.quantity = quantity
            
            if (Constants.USER_DEFAULT.string(forKey: "id")?.contains("guest"))! {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "signInStory") as! SignInViewController
                vc.delegate = self
                self.present(vc, animated: true)
            } else {
                loginSuccess()
            }
        }
    }
    
    func loginSuccess() {
        let item = Item(seq: "", itemId: "\(product.id!)_\(color)_\(size)", thumbId: String(product.thumbnails.split(separator: "|").first!), price: product.currentPrice, quantity: Int(quantity)!)
        let paymentVC = PaymentViewController(path: product.imgPath, items: [item])
        navigationController?.pushViewController(paymentVC, animated: true)
    }
}
