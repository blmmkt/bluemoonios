//
//  SignInViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 01/07/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

class SignInViewController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var loginExitBtn: UIButton!
    @IBOutlet weak var welcome1: UILabel!
    @IBOutlet weak var welcome2: UILabel!
    @IBOutlet weak var loginIdTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var loginPwTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var loginSignInBtn: UIButton!
    @IBOutlet weak var loginGuestBtn: UIButton!
    @IBOutlet weak var loginFindBtn: UIButton!
    @IBOutlet weak var optionLayout: UIView!
    @IBOutlet weak var loginFbFake: UIButton!
    @IBOutlet weak var loginSignUpBtn: UIButton!
    @IBOutlet weak var loginFbBtn: FBSDKLoginButton!
    
    weak var delegate: LoginDelegate!
    
    let dismissAlert = UIAlertAction(title: NSLocalizedString("complete_positive", comment: ""), style: .cancel, handler: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        set5sView()
        
        loginFbBtn.readPermissions = ["public_profile"]
        loginFbBtn.delegate = self
    }
    
    func set5sView() {
        if UIScreen.main.nativeBounds.height < 1334 {
            optionLayout.snp.makeConstraints { (make) in
                make.top.equalTo(loginGuestBtn.snp.bottom).offset(16)
            }
            loginIdTextField.snp.makeConstraints { (make) in
                make.top.equalTo(welcome2.snp.bottom).offset(16)
            }
            loginPwTextField.snp.makeConstraints { (make) in
                make.top.equalTo(loginIdTextField.snp.bottom).offset(16)
            }
        }
    }
    
    @IBAction func loginExitAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginSignInAction(_ sender: UIButton) {
        Alamofire.request(
            Constants.BASE_URL + "User_Login_Ok.jsp",
            method: .post,
            parameters: ["id": self.loginIdTextField.text ?? "",
                         "pw": self.loginPwTextField.text ?? ""],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResSignIn>) in
                if let result = response.result.value {
                    if(result.error_msg == "id error") {
                        let idAlert = UIAlertController.init(title: NSLocalizedString("id_error_title", comment: ""), message: NSLocalizedString("id_fail", comment: ""), preferredStyle: .alert)
                        idAlert.addAction(self.dismissAlert)
                        self.present(idAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "pw error") {
                        let pwAlert = UIAlertController.init(title: NSLocalizedString("pw_error_title", comment: ""), message: NSLocalizedString("pw_fail", comment: ""), preferredStyle: .alert)
                        pwAlert.addAction(self.dismissAlert)
                        self.present(pwAlert, animated: true, completion: nil)
                    } else if(result.error_msg == "ok") {
                        Constants.USER_DEFAULT.setValue(self.loginIdTextField.text, forKey: "id")
                        Constants.USER_DEFAULT.setValue(self.loginPwTextField.text, forKey: "pw")
                        Constants.USER_DEFAULT.setValue(result.token, forKey: "tkn")
                        Constants.USER_DEFAULT.setValue(false, forKey: "fb_login")
                        Constants.USER_DEFAULT.setValue(true, forKey: "login")
                        
                        self.loginExitBtn.sendActions(for: .touchUpInside)
                        self.delegate.loginSuccess()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    @IBAction func loginGuestAction(_ sender: UIButton) {
        loginExitBtn.sendActions(for: .touchUpInside)
        delegate.loginSuccess()
    }
    
    @IBAction func loginFbFakeAction(_ sender: UIButton) {
        loginFbBtn.sendActions(for: .touchUpInside)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(self.dismissAlert)
            present(alert, animated: true, completion: nil)
        } else if result.isCancelled {
            
        } else {
            if let loggedInToken = AccessToken.current {
                GraphRequest(graphPath: "me",
                             parameters: ["fields": "id, email"]).start { (response, results) -> Void in
                                
                                switch results {
                                case .failed:
                                    Constants.USER_DEFAULT.setValue(false, forKey: "fb_login")
                                    break;
                                case .success(let response):
                                    guard let id = response.dictionaryValue?["id"] as? String
                                        //                                    let email = response.dictionaryValue?["email"] as? String
                                        else {
                                            
                                            return
                                    }
                                    // 사용자 정보 조회 성공!
                                    self.checkFbLogIn(id: id, token: loggedInToken.authenticationToken)
                                }
                }
            } else {
                
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        let alert = UIAlertController(title: NSLocalizedString("Facebook logout", comment: ""), message: "Logout done", preferredStyle: .alert)
        alert.addAction(self.dismissAlert)
        present(alert, animated: true, completion: nil)
        
        Constants.USER_DEFAULT.setValue(false, forKey: "fb_login")
        Constants.USER_DEFAULT.setValue(false, forKey: "login")
    }
    
    
    @IBAction func showPrivacy(_ sender: UIButton) {
        guard let path = Bundle.main.path(forResource: "policy_en", ofType: "txt") else { return  }
        do {
            let content = try String(contentsOfFile: path, encoding: .utf8)
            
            let alert = UIAlertController(title: NSLocalizedString("policy_title", comment: ""), message: content, preferredStyle: .alert)
            alert.addAction(self.dismissAlert)
            present(alert, animated: true, completion: nil)
        } catch {
            
        }
    }
    
    func checkFbLogIn(id: String, token: String) {
        Alamofire.request(
            Constants.BASE_URL + "User_Chk_Id.jsp",
            method: .post,
            parameters: ["id": id],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.fbSignUp(id, token: token)
                        self.fbSignUpOfIn(id: id, token: token)
                    } else if(result.error_msg == "duplicate") {
                        self.fbSignUpOfIn(id: id, token: token)
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func fbSignUp(_ id: String, token: String) {
        Alamofire.request(
            Constants.BASE_URL + "User_Chk_Id.jsp",
            method: .post,
            parameters: ["id": id],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        Alamofire.request(
                            Constants.BASE_URL + "User_Insert_Token_Ok.jsp",
                            method: .post,
                            parameters: ["id": id,
                                         "fb_token": token],
                            encoding: URLEncoding.default,
                            headers: nil
                            )
                            .validate(statusCode: 200..<300)
                            .responseObject {(response: DataResponse<ResFind>) in
                                if let result = response.result.value {
                                    
                                }
                        }
                    }
                }
        }
    }
    
    func fbSignUpOfIn(id: String, token: String) {
        Constants.USER_DEFAULT.setValue(true, forKey: "fb_login")
        Constants.USER_DEFAULT.setValue(true, forKey: "login")
        Constants.USER_DEFAULT.setValue(id, forKey: "id")
        Constants.USER_DEFAULT.setValue(token, forKey: "tkn")
        
        loginExitBtn.sendActions(for: .touchUpInside)
        delegate.loginSuccess()
//        self.performSegue(withIdentifier: "goToMainSegue", sender: self)
    }
}
