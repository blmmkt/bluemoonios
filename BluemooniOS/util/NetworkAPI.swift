//
//  NetworkAPI.swift
//  BluemooniOS
//
//  Created by Soso on 01/05/2019.
//

import Foundation
import Alamofire

class NetworkAPI {
    
    class func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((DataResponse<ResFind>) -> Void)? = nil, onError: ((Error?) -> Void)? = nil) {
        let url = Constants.BASE_URL + endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseObject(completionHandler: { (result: DataResponse<ResFind>) in
                    print(result)
                    onCompletion?(result)
                })
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}
