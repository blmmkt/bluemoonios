//
//  UtilMethod.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 24/04/2019.
//

import UIKit

class UtilMethod: NSObject {
    static func getLocalPrice(priceString: String) -> Float {
        switch NSLocale.current.languageCode {
        case "en":
            return Float(priceString.split(separator: "|")[1])!
        case "ko":
            return Float(priceString.split(separator: "|")[0])!
        case "vi":
            return Float(priceString.split(separator: "|")[2])!
        default:
            return Float(priceString.split(separator: "|")[1])!
        }
    }
    
    static func getLocalUnit() -> String {
        switch NSLocale.current.languageCode {
        case "en":
            return "USD"
        case "ko":
            return "원"
        case "vi":
            return "VND"
        default:
            return "USD"
        }
    }
    
    static func getLocalCurrecyType() -> String {
        switch (NSLocale.current.languageCode) {
        case "en":
            return "US";
        case "ko":
            return "KR";
        case "vi":
            return "VN";
        default:
            return "";
        }
    }
    
    static func getLocalDeliveryFee() -> Float {
        switch NSLocale.current.languageCode {
        case "en":
            return 20
        case "ko":
            return 24000
        case "vi":
            return 500000
        default:
            return 20
        }
    }
    
    static func getLocalPointRatio() -> Float {
        switch NSLocale.current.languageCode {
        case "en":
            return 1 / 1200
        case "ko":
            return 1
        case "vi":
            return 20
        default:
            return 1 / 1200
        }
    }
    
    static func getOriginPrice(priceString: String, discount_rate: Int) -> Float {
        let price = getLocalPrice(priceString: priceString)
        return price * 100 / Float((100 - discount_rate))
    }
    
    static func getForattedPrice(price: Float) -> String {
        switch NSLocale.current.languageCode {
        case "en":
            return String.init(format: "%.2f", price)
        default:
            return String.init(format: "%.0f", price)
        }
    }
    
    static func changeSizeToNum(size: String) -> String {
        switch size {
        case "XS":
            return "00";
        case "S":
            return "01";
        case "M":
            return "02";
        case "L":
            return "03";
        case "XL":
            return "04";
        case "XXL":
            return "05";
        case "XXXL":
            return "06";
        case "F":
            return "07";
        default:
            return "";
        }
    }
    
    static func changeNumToSize(num: String) -> String {
        switch num {
        case "00":
            return "XS";
        case "01":
            return "S";
        case "02":
            return "M";
        case "03":
            return "L";
        case "04":
            return "XL";
        case "05":
            return "XXL";
        case "06":
            return "XXXL";
        case "07":
            return "F";
        default:
            return "";
        }
    }
    
    
    
    static func getLocalBirthDay(birth: String) -> String {
        let birthArray = Array(birth)
        let day = String(birthArray[6]) + String(birthArray[7])
        let month = String(birthArray[4]) + String(birthArray[5])
        let year = String(birthArray[0]) + String(birthArray[1]) + String(birthArray[2]) + String(birthArray[3])
        
        switch NSLocale.current.languageCode {
        case "en":
            return month + "-" + day + "-" + year
        case "ko":
            return year + "-" + month + "-" + day
        case "vi":
            return day + "-" + month + "-" + year
        default:
            return ""
        }
    }
}
