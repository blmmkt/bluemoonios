//
//  Constants.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 13/04/2019.
//

import UIKit

class Constants: NSObject {
    static let USER_DEFAULT = UserDefaults.standard
    
    static let BASE_URL: String = "https://www.blmmkt.com/bluemoon_app2019gfuid3jf7843/"
    
    static let COLOR_BASIC: UIColor = UIColor.init(red: 40/255, green: 42/255, blue: 80/255, alpha: 1)
    
}
