//
//  CustomButton.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 14/04/2019.
//

import UIKit
import SnapKit

class CustomButton: UIButton {
    
    let contentView: UIView = UIView()
    let basicButton: UIButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = self.frame.height / 2
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.white.cgColor
        contentView.layer.cornerRadius = (frame.height - 10) / 2
        contentView.isUserInteractionEnabled = false
        self.addSubview(contentView)
    }
    
    func setBasicInit() {
        self.layer.borderColor = Constants.COLOR_BASIC.cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = self.frame.height / 2
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        
        addSubview(basicButton)
        basicButton.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        basicButton.layer.borderWidth = 1
        basicButton.layer.borderColor = Constants.COLOR_BASIC.cgColor
        basicButton.layer.cornerRadius = (frame.height - 10) / 2
        basicButton.backgroundColor = Constants.COLOR_BASIC
        basicButton.setTitleColor(UIColor.white, for: .normal)
        basicButton.isUserInteractionEnabled = false
        self.addSubview(basicButton)
    }
    
    func basicTitle(_ title: String) {
        basicButton.setTitle(title, for: .normal)
        basicButton.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 22)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
