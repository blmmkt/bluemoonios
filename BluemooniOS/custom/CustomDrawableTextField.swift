//
//  CustomDrawableTextField.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 22/03/2019.
//

import UIKit

class CustomDrawableTextField: UIView {
    
    var imageView: UIImageView!
    var innerView: UIView!
    var textField: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit(margin: Int) {
        // init base uiView
        self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width - CGFloat(margin), height: 48)
//        self.layer.borderWidth = 3
//        self.layer.borderColor = UIColor.white.cgColor
//        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
        
        // add innerView
        innerView = UIView()
        innerView.frame = CGRect.init(x: 5, y: 5, width: UIScreen.main.bounds.width - CGFloat(margin) - 18.5, height: self.frame.height - 10)
//        innerView.layer.borderWidth = 1
//        innerView.layer.borderColor = UIColor.white.cgColor
//        innerView.layer.cornerRadius = innerView.frame.height / 2
        innerView.layer.masksToBounds = true
        innerView.backgroundColor = UIColor.clear
        self.addSubview(innerView)
        
        // add imageview
        self.imageView = UIImageView()
        self.imageView.frame = CGRect.init(x: 10, y: (innerView.bounds.height / 2 - 12.5), width: 25, height: 25)
        innerView.addSubview(self.imageView)
        
        // add TextField
        self.textField = UITextField()
        textField.frame = CGRect.init(x: imageView.frame.origin.x + imageView.bounds.width + 4, y: 0, width: innerView.bounds.width - imageView.frame.origin.x + imageView.bounds.width, height: innerView.bounds.height)
        textField.font = UIFont(name: "SCDream", size: 15)
        textField.textColor = UIColor.black
        
        if UIScreen.main.bounds.width < 380 {
            textField.font = textField.font?.withSize(12)
        } else {
            textField.font = textField.font?.withSize(15)
        }
        
        innerView.addSubview(textField)
        
        let underline = UIView()
        underline.backgroundColor = .black
        innerView.addSubview(underline)
        underline.snp.makeConstraints { (make) in
            make.bottom.right.equalToSuperview()
            make.left.equalTo(self.imageView).offset(self.imageView.frame.width + 4)
            make.height.equalTo(1)
        }
    }
    
    func setImageSrc(imageName: String) {
        self.imageView.image = UIImage.init(named: imageName)
    }
    
    func setEditable(_ boolean: Bool) {
        self.textField.isEnabled = false
    }
}
