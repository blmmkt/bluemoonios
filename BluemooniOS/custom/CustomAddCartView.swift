//
//  CustomAddCartView.swift
//  BluemooniOS
//
//  Created by Soso on 01/05/2019.
//

import UIKit
import SnapKit
import Alamofire

protocol CustomAddCartViewProtocol: class {
    func didPressLike(state: Bool)
    func didPressToggle(state: Bool, sender: UIButton)
    func didPressDone(size: String, color: String, quantity: String)
}

class CustomAddCartView: UIView {
    weak var delegate: CustomAddCartViewProtocol?
    
    var isLiked: Bool = false
    var isOpened: Bool = false
    var isTease: Bool = false
    
    var product: Product
    
    var sizes: [String] = []
    var colors: [String] = []
    
    var maxQuantity: Int = 0 {
        didSet {
            maxQuantityLabel.text = NSLocalizedString("max_quantity", comment: "") + "\(maxQuantity)"
            if quantity > maxQuantity {
                quantity = maxQuantity
            }
        }
    }
    var quantity: Int = 0 {
        didSet {
            quantityLabel.text = "\(quantity)"
            priceLabel.attributedText = product.totalPriceText(quantity: quantity)
        }
    }
    
    init(product: Product) {
        self.product = product
        self.isLiked = product.my_like
        
        super.init(frame: .zero)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let tabBarHeight: CGFloat = 50
//    static let viewHeight: CGFloat = 400
    
    lazy var likeButton: UIButton = {
        let view = UIButton()
        view.backgroundColor = .black
//            Constants.COLOR_BASIC
        view.setImage(self.isLiked ? #imageLiteral(resourceName: "heart-filled") : #imageLiteral(resourceName: "heart-empty") , for: .normal)
        return view
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.alignment = .fill
        view.distribution = .fillEqually
        return view
    }()
    
    lazy var cartButton: CustomImageButton = {
        let view = CustomImageButton()
        view.customImageView.image = #imageLiteral(resourceName: "icon_cart_white")
        view.customTitleLabel.text = "Add to Cart"
        view.backgroundColor = .black
//            Constants.COLOR_BASIC
        view.addTarget(self, action: #selector(handleToggle(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var buyButton: CustomImageButton = {
        let view = CustomImageButton()
        view.customImageView.image = #imageLiteral(resourceName: "icon_buy_white")
        view.customTitleLabel.text = "Buy"
        view.backgroundColor = .black
//            Constants.COLOR_BASIC
        view.addTarget(self, action: #selector(handleToggle(_:)), for: .touchUpInside)
        return view
    }()
    
    lazy var sizePickerView: CustomPickerView = {
        let view = CustomPickerView(frame: .zero)
        view.cornerRadius = 20
        view.pickerView.delegate = self
        view.pickerView.dataSource = self
        if UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0 {
            view.backgroundColor = .black
//                Constants.COLOR_BASIC
            view.borderColor = .black
//                Constants.COLOR_BASIC
        }
        return view
    }()
    
    lazy var colorPickerView: CustomPickerView = {
        let view = CustomPickerView(frame: .zero)
        view.cornerRadius = 20
        view.pickerView.delegate = self
        view.pickerView.dataSource = self
        return view
    }()
    
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hex: 0xEEEEEE)
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.text = product.name
        return view
    }()
    
    let decreaseButton: UIButton = {
        let view = UIButton()
        view.setTitle("-", for: .normal)
        view.setTitleColor(.black, for: .normal)
        view.addTarget(self, action: #selector(handleDecrease), for: .touchUpInside)
        view.backgroundColor = .white
        view.shadowColor = .black
        view.shadowOffset = CGSize(width: 0, height: 1)
        view.shadowRadius = 1
        view.shadowOpacity = 1
        view.clipsToBounds = false
        return view
    }()
    
    let increaseButton: UIButton = {
        let view = UIButton()
        view.setTitle("+", for: .normal)
        view.setTitleColor(.black, for: .normal)
        view.addTarget(self, action: #selector(handleIncrease), for: .touchUpInside)
        view.backgroundColor = .white
        view.shadowColor = .black
        view.shadowOffset = CGSize(width: 0, height: 1)
        view.shadowRadius = 1
        view.shadowOpacity = 1
        view.clipsToBounds = false
        return view
    }()
    
    let quantityLabel: UILabel = {
        let view = UILabel()
        view.text = "1"
        view.textAlignment = .center
        view.backgroundColor = .white
        return view
    }()
    
    let maxQuantityLabel: UILabel = {
        let view = UILabel()
        view.textColor = .lightGray
        return view
    }()
    
    let priceLabel: UILabel = {
        let view = UILabel()
        return view
    }()
    
    let buttonView: UIView = {
        let view = UIView()
        view.borderColor = UIColor.clear
//            Constants.COLOR_BASIC
//        view.borderWidth = 4
//        view.cornerRadius = 32
        return view
    }()
    
    let doneButton: UIButton = {
        let view = UIButton()
        view.setTitle(NSLocalizedString("done", comment: ""), for: .normal)
        view.setTitleColor(.white, for: .normal)
        view.addTarget(self, action: #selector(handleDone), for: .touchUpInside)
        view.backgroundColor = .black
//            Constants.COLOR_BASIC
//        view.borderWidth = 1
//        view.cornerRadius = 25
        return view
    }()
    
    func setupView() {
        if UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0 > 0 {
            backgroundColor = .black
//                Constants.COLOR_BASIC
        } else {
            backgroundColor = .white
        }
        
        addSubview(likeButton)
        likeButton.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview()
            make.height.width.equalTo(CustomAddCartView.tabBarHeight)
        }
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.right.equalToSuperview()
            make.left.equalTo(likeButton.snp.right)
            make.bottom.equalTo(likeButton)
        }
        stackView.addArrangedSubview(cartButton)
        if product is ResSales.Sale {
            stackView.addArrangedSubview(buyButton)
        }
        addSubview(separatorView)
        separatorView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalTo(likeButton)
            make.width.equalTo(0.5)
        }
        addSubview(sizePickerView)
        sizePickerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(stackView.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(50)
        }
        addSubview(colorPickerView)
        colorPickerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(sizePickerView.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(50)
        }
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.top.equalTo(colorPickerView.snp.bottom).offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
            make.height.equalTo(30)
        }
        contentView.addSubview(decreaseButton)
        decreaseButton.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.top.equalTo(nameLabel.snp.bottom)
            make.height.width.equalTo(30)
        }
        contentView.addSubview(quantityLabel)
        quantityLabel.snp.makeConstraints { (make) in
            make.left.equalTo(decreaseButton.snp.right)
            make.top.height.equalTo(decreaseButton)
            make.width.equalTo(40)
        }
        contentView.addSubview(increaseButton)
        increaseButton.snp.makeConstraints { (make) in
            make.left.equalTo(quantityLabel.snp.right)
            make.top.height.equalTo(quantityLabel)
            make.width.equalTo(decreaseButton)
        }
        contentView.addSubview(priceLabel)
        priceLabel.snp.makeConstraints { (make) in
            make.top.height.equalTo(increaseButton)
            make.right.equalToSuperview().offset(-5)
        }
        contentView.addSubview(maxQuantityLabel)
        maxQuantityLabel.snp.makeConstraints { (make) in
            make.left.equalTo(decreaseButton)
            make.top.equalTo(decreaseButton.snp.bottom)
            make.bottom.equalToSuperview().offset(-5)
            make.height.equalTo(30)
        }
        addSubview(buttonView)
        buttonView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView.snp.bottom).offset(20)
            make.bottom.equalToSuperview().offset(-20)
            make.centerX.equalToSuperview()
        }
        buttonView.addSubview(doneButton)
        doneButton.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(7)
            make.right.bottom.equalToSuperview().offset(-7)
            make.height.equalTo(50)
            make.width.equalTo(170)
        }
    }
    
    func setupData(sizes: [String], colors: [String]) {
        self.sizes = sizes
        self.colors = colors
        if let size = sizes.first, let color = colors.first {
            sizePickerView.label.text = size
            colorPickerView.label.text = color
            nameLabel.text = product.name + " | " + size + " | " + color
            quantity = 1
            getProductQuantity(size: UtilMethod.changeSizeToNum(size: size), color: color)
        }
    }
    
    @objc func handleToggle(_ sender: UIButton) {
        isOpened = !isOpened
        delegate?.didPressToggle(state: isOpened, sender: sender)
    }
    
    @objc func handleIncrease() {
        if quantity < maxQuantity {
            quantity += 1
        }
    }
    
    @objc func handleDecrease() {
        if quantity > 1 {
            quantity -= 1
        }
    }
    
    @objc func handleDone() {
        delegate?.didPressDone(size: UtilMethod.changeSizeToNum(size: sizes[sizePickerView.pickerView.selectedRow(inComponent: 0)]),
                               color: colors[colorPickerView.pickerView.selectedRow(inComponent: 0)],
                               quantity: String(quantity))
    }
    
    func getProductQuantity(size: String, color: String) {
        let params = [
            "parent_id": product.id!,
            "size": size,
            "color": color
        ]
        print(params)
        Alamofire.request(
            Constants.BASE_URL + "Product_Chk_Count.jsp",
            method: .post,
            parameters: [
                "parent_id": product.id!,
                "size": size,
                "color": color
            ],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseJSON(completionHandler: { (response) in
                print(response)
            })
            .responseObject(completionHandler: { [weak self] (response: DataResponse<ResQuantity>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self?.maxQuantity = Int(result.quantity) ?? 0
                    } else {
                        print(result.error_msg)
                    }
                }
            })
    }
    
}

extension CustomAddCartView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case sizePickerView.pickerView:
            return 1
        case colorPickerView.pickerView:
            return 1
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case sizePickerView.pickerView:
            return sizes.count
        case colorPickerView.pickerView:
            return colors.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case sizePickerView.pickerView:
            return sizes[row]
        case colorPickerView.pickerView:
            return colors[row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case sizePickerView.pickerView:
            sizePickerView.label.text = sizes[row]
        case colorPickerView.pickerView:
            colorPickerView.label.text = colors[row]
        default:
            break
        }
        let size = sizes[sizePickerView.pickerView.selectedRow(inComponent: 0)]
        let color = colors[colorPickerView.pickerView.selectedRow(inComponent: 0)]
        nameLabel.text = product.name + " | " + size + " | " + color
        getProductQuantity(size: UtilMethod.changeSizeToNum(size: size), color: color)
    }
    
}
