//
//  OfficeInformationView.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 15/04/2019.
//

import UIKit

class OfficeInformationView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let information = UILabel();
        information.frame = CGRect.init(x: 10, y: 10, width: self.bounds.width - 10, height: self.bounds.height - 10)
        information.text = NSLocalizedString("office_info", comment: "")
        information.numberOfLines = 6
        if UIScreen.main.bounds.width < 350 {
            information.font = UIFont.systemFont(ofSize: 11)
        } else {
            information.font = UIFont.systemFont(ofSize: 9)
        }
        self.addSubview(information)
        
        self.backgroundColor = UIColor.init(red: 187/255, green: 187/255, blue: 187/255, alpha: 1)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
