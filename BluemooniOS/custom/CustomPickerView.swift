//
//  CustomPickerView.swift
//  BluemooniOS
//
//  Created by Soso on 29/04/2019.
//

import UIKit
import SnapKit

class CustomPickerView: UIView {
    var textField: UITextField = UITextField()
    var label: UILabel = UILabel()
    var button: UIButton = UIButton()
    var stackView: UIStackView = UIStackView()
    var imageView: UIImageView = UIImageView()
    var pickerView: UIPickerView = UIPickerView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit() {
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = frame.height / 2
        backgroundColor = UIColor.white
        
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(imageView)
        imageView.image = #imageLiteral(resourceName: "button_down")
        addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        button.addTarget(self, action: #selector(handleClick), for: .touchUpInside)
        addSubview(textField)
        textField.isHidden = true
        textField.inputView = pickerView
        let toolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        let done = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(handleDismiss))
        toolbar.semanticContentAttribute = .forceRightToLeft
        toolbar.items = [done]
        textField.inputAccessoryView = toolbar
        pickerView.backgroundColor = .white
    }
    
    @objc func handleClick() {
        textField.becomeFirstResponder()
    }
    
    @objc func handleDismiss() {
        textField.resignFirstResponder()
    }
    
}
