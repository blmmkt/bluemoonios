//
//  CustomLabelTextField.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 13/04/2019.
//

import UIKit

class CustomLabelTextField: UIView {

    var text: UILabel!
    var textField: UITextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width - 32, height: 48)
        self.layer.borderWidth = 3
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
        
        let innerView = UIView()
        innerView.frame = CGRect.init(x: 5, y: 5, width: self.bounds.width - 10, height: self.bounds.height - 10)
        innerView.layer.borderWidth = 1
        innerView.layer.borderColor = UIColor.white.cgColor
        innerView.layer.cornerRadius = innerView.frame.height / 2
        innerView.layer.masksToBounds = true
        self.addSubview(innerView)
        
        let whiteBack = UIView()
        whiteBack.frame = CGRect.init(x: 0, y: 0, width: innerView.frame.width / 6, height: innerView.bounds.height)
        whiteBack.backgroundColor = UIColor.white
        whiteBack.layer.masksToBounds = true
        innerView.addSubview(whiteBack)
        
        self.text = UILabel()
        text.textColor = UIColor.init(red: 40 / 255, green: 42 / 255, blue: 80 / 255, alpha: 1)
        text.font = text.font.withSize(22)
        text.frame = CGRect.init(x: whiteBack.frame.width / 4, y: 0, width: 80, height: whiteBack.frame.height)
        whiteBack.addSubview(text)
        
        self.textField = UITextField()
        textField.frame = CGRect.init(x: whiteBack.frame.origin.x + whiteBack.bounds.width + 10, y: 0, width: innerView.bounds.width - whiteBack.frame.origin.x + whiteBack.bounds.width, height: innerView.bounds.height)
        textField.textColor = UIColor.white
        innerView.addSubview(textField)
    }
    
    func setLabel(label: String) {
        self.text.text = label
    }
    
    func setInputType(inputType: UIKeyboardType) {
        self.textField.keyboardType = inputType
    }
    
    func setSecureText(isSecure: Bool) {
        self.textField.isSecureTextEntry = isSecure
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
