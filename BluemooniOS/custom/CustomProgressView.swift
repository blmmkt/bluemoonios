//
//  CustomProgressView.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 27/04/2019.
//

import UIKit

class CustomProgressView: UIProgressView {
    
    var realProgress: UIView!
    var secondary: UIView!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.progress = 0
    }
    
    func setSecondaryProgress(progress: Float) {
        secondary = UIView()
        secondary.frame = CGRect.init(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(Float(self.frame.width) * progress), height: 5)
        secondary.backgroundColor = UIColor.purple
        self.addSubview(secondary)
    }
    
    override func setProgress(_ progress: Float, animated: Bool) {
        realProgress = UIView()
        realProgress.frame = CGRect.init(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(Float(self.frame.width) * progress), height: 5)
        realProgress.backgroundColor = UIColor.blue
        self.addSubview(realProgress)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
