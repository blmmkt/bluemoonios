//
//  CustomDatePicker.swift
//  BluemooniOS
//
//  Created by Soso on 30/04/2019.
//

import UIKit
import SnapKit

class CustomDatePicker: UIView {
    var label: UILabel = UILabel()
    var contentView: UIView = UIView()
    var stackView: UIStackView = UIStackView()
    var datePicker: UIDatePicker = UIDatePicker()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit() {
//        layer.borderWidth = 3
//        layer.borderColor = UIColor.white.cgColor
//        layer.cornerRadius = 18
        backgroundColor = UIColor.clear
        
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.right.bottom.equalToSuperview()
        }
//        contentView.layer.borderWidth = 1
//        contentView.layer.borderColor = UIColor.black.cgColor
//        contentView.layer.cornerRadius = 14
        stackView.frame = CGRect.init(x: 0, y: 5, width: frame.width, height: frame.height)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.clipsToBounds = true
        stackView.layer.masksToBounds = true
        label.text = "Birth"
        label.textColor = .black
        label.widthAnchor.constraint(equalToConstant: 40).isActive = true
        stackView.addArrangedSubview(label)
        datePicker.frame = CGRect(x: -30, y: 0, width: frame.width - 50, height: frame.height - 10)
        datePicker.clipsToBounds = true
        datePicker.layer.masksToBounds = true
        datePicker.date = Date()
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .clear
        datePicker.setValue(UIColor.black, forKeyPath: "textColor")
        datePicker.subviews[0].subviews[1].isHidden = true
        datePicker.subviews[0].subviews[2].isHidden = true

        stackView.addArrangedSubview(datePicker)
        addSubview(stackView)
    }
    
}

