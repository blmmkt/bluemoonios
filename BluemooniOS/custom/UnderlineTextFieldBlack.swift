//
//  UnderlineTextFieldBlack.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 02/07/2019.
//

import UIKit

class UnderlineTextFieldBlack: UITextField {

    var underline: UIView = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(underline)
        
        underline.backgroundColor = UIColor.black
        underline.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.right.bottom.equalToSuperview()
        }
        
//        underline.frame = CGRect.init(x: 0, y: self.frame.height, width: self.frame.width, height: 1)
//        underline.backgroundColor = UIColor.black
    }
}
