//
//  CustomRadioButtonView.swift
//  BluemooniOS
//
//  Created by Soso on 30/04/2019.
//

import UIKit

class CustomRadioButtonView: UIView {
    var label: UILabel = UILabel()
    var contentView: UIView = UIView()
    var stackView: UIStackView = UIStackView()
    var buttons: [UIButton] = []
    
    var selectedButton: UIButton? {
        for button in buttons {
            if button.isSelected {
                return button
            }
        }
        return nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit(title: String, titles: [String]) {
//        layer.borderWidth = 3
//        layer.borderColor = UIColor.white.cgColor
//        layer.cornerRadius = frame.height / 2
        backgroundColor = UIColor.clear
        
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
//        contentView.layer.borderWidth = 1
//        contentView.layer.borderColor = UIColor.white.cgColor
//        contentView.layer.cornerRadius = (frame.height - 10) / 2
        let underline = UIView()
        underline.backgroundColor = .black
        addSubview(underline)
        underline.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.bottom.right.equalToSuperview()
        }
        
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(5)
            make.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        label.text = title
        label.textColor = .black
        label.widthAnchor.constraint(equalToConstant: 60).isActive = true
        stackView.addArrangedSubview(label)
        for i in 0..<titles.count {
            let button = UIButton()
            button.setTitle(titles[i], for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
            button.setImage(#imageLiteral(resourceName: "circle-unchecked-black"), for: .normal)
            button.setImage(#imageLiteral(resourceName: "circle-checked-black"), for: .selected)
            button.addTarget(self, action: #selector(handlePress(_:)), for: .touchUpInside)
            button.semanticContentAttribute = .forceRightToLeft
            button.imageView?.contentMode = .scaleAspectFit
            button.heightAnchor.constraint(equalToConstant: 25).isActive = true
            button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            buttons.append(button)
            stackView.addArrangedSubview(button)
        }
    }
    
    @objc func handlePress(_ sender: UIButton) {
        print("in")
        for button in buttons {
            button.isSelected = button == sender
        }
    }
}
