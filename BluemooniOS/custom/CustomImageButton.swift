//
//  CustomImageButton.swift
//  BluemooniOS
//
//  Created by Soso on 04/05/2019.
//

import UIKit

class CustomImageButton: UIButton {
    var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fillProportionally
        view.isUserInteractionEnabled = false
        return view
    }()
    var customImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    var customTitleLabel: UILabel = {
        let view = UILabel()
        view.textColor = .white
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setInit() {
        addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
        stackView.addArrangedSubview(customImageView)
        stackView.addArrangedSubview(customTitleLabel)
    }
    
}
