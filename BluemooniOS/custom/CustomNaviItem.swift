//
//  CustomNaviItem.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 20/04/2019.
//

import UIKit

class CustomNaviItem: UIButton {

//    var icon: UIImage!
//    var title: String!
    var iconView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
        fatalError("init? is not declared")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        iconView = UIImageView()
        iconView.frame = CGRect.init(x: 8, y: 0, width: 36, height: 36)
        iconView.contentMode = .scaleAspectFit
        self.addSubview(iconView)
    }
    
    func setIcon(image: String) {
        self.iconView.image = UIImage(named: image)
    }
    
    func setTitle(title: String) {
        super.setTitle(title, for: .normal)
        super.setTitleColor(UIColor.black, for: .normal)
        super.setTitleColor(UIColor.gray, for: .highlighted)
        super.contentHorizontalAlignment = .left
        self.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 76, bottom: 0, right: 0)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
