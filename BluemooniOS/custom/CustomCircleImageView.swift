//
//  CustomCircleImageView.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit

class CustomCircleImageView: UIView {

    var realImage: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.frame = CGRect.init(x: 0, y: 0, width: 96, height: 96)
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = 48
        self.backgroundColor = UIColor.clear
        
        realImage = UIImageView()
        realImage.frame = CGRect.init(x: 5, y: 5, width: 86, height: 86)
        realImage.layer.borderColor = UIColor.black.cgColor
        realImage.layer.borderWidth = 1
        realImage.layer.cornerRadius = 43
//        realImage.image = UIImage(named: "icon_profile_white")
        realImage.clipsToBounds = true
        self.addSubview(realImage)
    }
    
    func changeProfileImage(image: UIImage) {
        realImage.image = image
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
