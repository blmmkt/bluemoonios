//
//  EditAddressViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 20/05/2019.
//

import UIKit

class EditAddressViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    let provinces: [String] = [
        "An Giang", "Bà Rịa-Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Dương", "Bình Phước", "Bình Thuận", "Bình Định", "Cà Mau", "Cần Thơ", "Cao Bằng", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Nội", "Hà Tĩnh", "Hải Dương", "Hải Phòng", "Hậu Giang", "Hồ Chí Minh", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú yên", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên-Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Diễn Biến", "Đồng Nai", "Đồng Tháp"
    ]
    let cities: [[String]] = [
        ["An Phú", "Châu Đốc", "Châu Phú", "Châu Thành", "Chợ Mới", "Long Xuyên", "Phú Tân", "Tân Châu", "Thoại Sơn", "Tịnh Biên", "Tri Tôn"],
        ["Bà Rịa", "Châu Đức", "Côn Đảo", "Đặt Đồ", "Long Điền", "Tân Thành", "Vũng Tàu", "Xuyên Mộc"],
        ["Bắc Giang", "Hiệp Hòa", "Lạng Giang", "Lục Nam", "Lục Ngạn", "Sơn Đông", "Tân Yên", "Việt Yên", "Yên Dũng", "Yên Thế"],
        ["Ba Bể", "Bạch Thông", "Bắc Kạn", "Chợ Đồn", "Chợ Mới", "Na Rì", "Ngân Sơn", "Pác Nặm"],
        ["Bạc Liêu", "Giá Rai", "Đông Hải", "Hòa Binh", "Hồng Dân", "Phước Long", "Vĩnh Lợi"],
        ["Bắc Ninh", "Gia Bình", "Lương Tài", "Quế Võ", "Thuận Thành", "Tiên Du", "Từ Sơn", "Yên Phong"],
        ["Ba Tri", "Bến Tre", "Bình Đại", "Châu Thành", "Chợ Lách", "Giồng Trôm", "Mỏ Cày Bắc", "Mỏ Cày Nam", "Thạnh Phú"],
        ["Bến Cát", "Dầu Tiếng", "Dĩ An", "Phú Giáo", "Tân Uyên", "Thủ Dầu Một", "Thuận An"],
        ["Bình Long", "Bù Đăng", "Bù Đốp", "Chơn Thành", "Đồng Phú", "Đồng Xoài", "Lộc Ninh", "Phước Long"],
        ["Bắc Bình", "Đức Linh", "Hàm Tân", "Hàm Thuận Bắc", "Hàm Thuận Nam", "La Gi", "Phan Thiết", "Phú Quý", "Tánh Linh", "Tuy Phong"],
        ["An Lão", "An Nhơn", "Hoài Ân", "Hoài Nhơn", "Phù Cát", "Phù Mỹ", "Quy Nhơn", "Tây Sơn", "Tuy Phước", "Vân Canh", "Vĩnh Thạnh"],
        ["Cà Mau", "Cái Nước", "Đầm Dơi", "Năm Căn", "Ngọc Hiển", "Phú Tân", "Thới Bình", "Trần Văn Thời", "U Minh"],
        ["Bình Thủy", "Cái Răng", "Cờ Đỏ", "Ninh Kiều", "Ô Môn", "Phong Điền", "Thốt Nốt", "Vĩnh Thạnh"],
        ["Bảo Lạc", "Bảo Lâm", "Cao Bằng", "Hà Quảng", "Hạ Lang", "Hòa An", "Nguyên Bình", "Phục Hòa", "Quảng Uyên", "Thạch An", "Thông Nông", "Trà Lĩnh", "Trùng Khánh"],
        ["An Khê", "Ayun Pa", "Chư Păh", "Chư Prông", "Chư Sê", "Đắk Đoa", "Đắk Pơ", "Đức Cơ", "Ia Grai", "Ia Pa", "K\'Bang", "Kông Chro", "Krông Pa", "Mang Yang", "Phú Thiện", "Pleiku"],
        ["Bắc Mê", "Bắc Quang", "Đồng Văn", "Hà Giang", "Hoàng Su Phì", "Mèo Vạc", "Quản Bạ", "Quảng Bình", "Vị Xuyên", "Xín Mần", "Yên Minh"],
        ["Bình Lục", "Duy Tiên", "Kim Bảng", "Lý Nhân", "Phủ Lý", "Thanh Liêm"],
        ["Ba Đình", "Cầu Giấy", "Đông Anh", "Đống Đa", "Gia Lâm", "Hai Bà Trưng", "Hoàn Kiếm", "Hoàng Mai", "Long Biên", "Sóc Sơn", "Tây Hồ", "Thanh Trì", "Thanh Xuân", "Từ Liêm", "Ba Vì", "Chương Mỹ", "Đan Phượng", "Hà Đông", "Hoài Đức", "Mỹ Đức", "Phú Xuyên", "Phúc Thọ", "Quốc Oai", "Sơn Tây", "Thạch Thất", "Thanh Oai", "Thường Tín", "Ứng Hòa", "Mê Linh"],
        ["Can Lộc", "Cẩm Xuyên", "Đức Thọ", "Hà Tĩnh", "Hồng Lĩnh", "Hương Khê", "Hương Sơn", "Kỳ Anh", "Lộc Hà", "Nghi Xuân", "Thạch Hà", "Vũ Quang"],
        ["Bình Giang", "Cẩm Giàng", "Chí Linh", "Gia Lộc", "Hải Dương", "Kim Thành", "Kinh Môn", "Nam Sách", "Ninh Giang", "Thanh Hà", "Thanh Miện", "Tứ Kỳ"],
        ["An Dương", "An Lão", "Bạch Long Vĩ", "Cát Hải", "Dương Kinh", "Đồ Sơn", "Hồng Bàng", "Kiến An", "Kiến Thụy", "Lê Chân", "Ngô Quyền", "Thủy Nguyên", "Tiên Lãng", "Vĩnh Bảo", "Hải An"],
        ["Châu Thành", "Châu Thành A", "Long Mỹ", "Phụng Hiệp", "Ngã Bảy (formerly Tân Hiệp)", "Vị Thanh", "Vị Thủy"],
        ["Bình Chánh", "Bình Tân", "Bình Thạnh", "Cần Giờ", "Củ Chi", "Gò Vấp", "Hóc Môn", "Nhà Bè", "Phú Nhuận", "Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5", "Quận 6", "Quận 7", "Quận 8", "Quận 9", "Quận 10", "Quận 11", "Quận 12", "Tân Bình", "Tân Phú", "Thủ Đức"],
        ["Cao Phong", "Đà Bắc", "Hòa Bình", "Kim Bôi", "Kỳ Sơn", "Lạc Sơn", "Lạc Thủy", "Lương Sơn", "Mai Châu", "Tân Lạc", "Yên Thủy"],
        ["Ân Thi", "Hưng Yên", "Khoái Châu", "Kim Động", "Mỹ Hào", "Phù Cừ", "Tiên Lữ", "Văn Giang", "Văn Lâm", "Yên Mỹ"],
        ["Cam Lâm", "Cam Ranh", "Diên Khánh", "Khánh Sơn", "Khánh Vĩnh", "Nha Trang", "Ninh Hòa", "Trường Sa", "Vạn Ninh"],
        ["An Biên", "An Minh", "Châu Thành", "Giồng Riềng", "Gò Quao", "Hà Tiên", "Hòn Đất", "Kiên Hải", "Kiên Lương", "Phú Quốc", "Rạch Giá", "Tân Hiệp", "U Minh Thượng", "Vĩnh Thuận"],
        ["Đắk Glei", "Đắk Hà", "Đắk Tô", "Kon Plông", "Kon Rẫy", "Kon Tum", "Ngọc Hồi", "Sa Thầy", "Tu Mơ Rông"],
        ["Lai Châu", "Mường Tè", "Phong Thổ", "Sìn Hồ", "Tam Đường", "Than Uyên"],
        ["Bảo Lâm", "Bảo Lộc", "Cát Tiên", "Di Linh", "Da Lat", "Đạ Huoai", "Đạ Tẻh", "Đam Rông", "Đơn Dương", "Đức Trọng", "Lạc Dương", "Lâm Hà"],
        ["Bắc Sơn", "Bình Gia", "Cao Lộc", "Chi Lăng", "Đình Lập", "Hữu Lũng", "Lạng Sơn", "Lộc Bình", "Tràng Định", "Văn Lãng", "Văn Quan"],
        ["Bảo Thắng", "Bảo Yên", "Bát Xát", "Bắc Hà", "Lào Cai", "Mường Khương", "Sa Pa", "Si Ma Cai", "Văn Bàn"],
        ["Bến Lức", "Cần Đước", "Cần Giuộc", "Châu Thành", "Đức Hòa", "Đức Huệ", "Mộc Hóa", "Tân An", "Tân Hưng", "Tân Thạnh", "Tân Trụ", "Thạnh Hóa", "Thủ Thừa", "Vĩnh Hưng"],
        ["Giao Thủy", "Hải Hậu", "Mỹ Lộc", "Nam Định", "Nam Trực", "Nghĩa Hưng", "Trực Ninh", "Vụ Bản", "Xuân Trường", "Ý Yên"],
        ["Anh Sơn", "Con Cuông", "Cửa Lò", "Diễn Châu", "Đô Lương", "Hưng Nguyên", "Kỳ Sơn", "Nam Đàn", "Nghi Lộc", "Nghĩa Đàn", "Quế Phong", "Quỳ Châu", "Quỳ Hợp", "Quỳnh Lưu", "Tân Kỳ", "Thanh Chương", "Tương Dương", "Vinh", "Yên Thành"],
        ["Gia Viễn", "Hoa Lư", "Kim Sơn", "Nho Quan", "Ninh Bình", "Tam Điệp", "Yên Khánh", "Yên Mô"],
        ["Bác Ái", "Ninh Hải", "Ninh Phước", "Ninh Sơn", "Phan Rang–Tháp Chàm", "Thuận Bắc", "Thuận Nam"],
        ["Cẩm Khê", "Đoan Hùng", "Hạ Hòa", "Lâm Thao", "Phú Thọ", "Phù Ninh", "Tam Nông", "Tân Sơn", "Thanh Ba", "Thanh Sơn", "Thanh Thủy", "Việt Trì", "Yên Lập"],
        ["Đông Hòa", "Đồng Xuân", "Phú Hòa", "Sông Cầu", "Sông Hinh", "Sơn Hòa", "Tây Hòa", "Tuy An", "Tuy Hòa"],
        ["Bố Trạch", "Đồng Hới", "Lệ Thủy", "Minh Hóa", "Quảng Ninh", "Quảng Trạch", "Tuyên Hóa"],
        ["Bắc Trà My", "Duy Xuyên", "Đại Lộc", "Điện Bàn", "Đông Giang", "Hiệp Đức", "Hội An", "Nam Giang", "Nam Trà My", "Núi Thành", "Phú Ninh", "Phước Sơn", "Quế Sơn", "Tam Kỳ", "Tây Giang", "Thăng Bình", "Tiên Phước", "Nông Sơn"],
        ["Ba Tơ", "Bình Sơn", "Đức Phổ", "Lý Sơn", "Minh Long", "Mộ Đức", "Nghĩa Hành", "Sơn Hà", "Sơn Tây", "Sơn Tịnh", "Tây Trà", "Trà Bồng", "Tư Nghĩa", "Quảng Ngãi"],
        ["Ba Chẽ", "Bình Liêu", "Cẩm Phả", "Cô Tô", "Đầm Hà", "Đông Triều", "Hạ Long", "Hải Hà", "Hoành Bồ", "Móng Cái", "Tiên Yên", "Uông Bí", "Vân Đồn", "Quảng Yên"],
        ["Cam Lộ", "Cồn Cỏ", "Đa Krông", "Đông Hà", "Gio Linh", "Hải Lăng", "Hướng Hóa", "Quảng Trị", "Triệu Phong", "Vĩnh Linh"],
        ["Châu Thành", "Cù Lao Dung", "Kế Sách", "Long Phú", "Mỹ Tú", "Mỹ Xuyên", "Ngã Năm", "Sóc Trăng", "Thạnh Trị", "Trần Đề", "Vĩnh Châu"],
        ["Bắc Yên", "Mai Sơn", "Mộc Châu", "Mường La", "Phù Yên", "Quỳnh Nhai", "Sông Mã", "Sốp Cộp", "Sơn La", "Thuận Châu", "Yên Châu"],
        ["Bến Cầu", "Châu Thành", "Dương Minh Châu", "Gò Dầu", "Hòa Thành", "Tân Biên", "Tân Châu", "Tây Ninh", "Trảng Bàng"],
        ["Đông Hưng", "Hưng Hà", "Kiến Xương", "Quỳnh Phụ", "Thái Bình", "Thái Thụy", "Tiền Hải", "Vũ Thư"],
        ["Đại Từ", "Định Hóa", "Đồng Hỷ", "Phổ Yên", "Phú Bình", "Phú Lương", "Sông Công", "Thái Nguyên", "Võ Nhai"],
        ["Bá Thước", "Bỉm Sơn", "Cẩm Thủy", "Đông Sơn", "Hà Trung", "Hậu Lộc", "Hoằng Hóa", "Lang Chánh", "Mường Lát", "Nga Sơn", "Ngọc Lặc", "Như Thanh", "Như Xuân", "Nông Cống", "Quan Hóa", "Quan Sơn", "Quảng Xương", "Sầm Sơn", "Thạch Thành", "Thanh Hóa", "Thiệu Hóa", "Thọ Xuân", "Thường Xuân", "Tĩnh Gia", "Triệu Sơn", "Vĩnh Lộc", "Yên Định"],
        ["A Lưới", "Huế", "Hương Thủy", "Hương Trà", "Nam Đông", "Phong Điền", "Phú Lộc", "Phú Vang", "Quảng Điền"],
        ["Cái Bè", "Cai Lậy", "Châu Thành", "Chợ Gạo", "Gò Công", "Gò Công Dông", "Gò Công Tây", "Mỹ Tho", "Tân Phú Đông", "Tân Phước"],
        ["Càng Long", "Cầu Kè", "Cầu Ngang", "Châu Thành", "Duyên Hải", "Tiểu Cần", "Trà Cú", "Trà Vinh"],
        ["Chiêm Hóa", "Hàm Yên", "Lâm Bình", "Nà Hang", "Sơn Dương", "Tuyên Quang", "Yên Sơn"],
        ["Bình Minh", "Bình Tân", "Long Hồ", "Mang Thít", "Tâm Bình", "Trà Ôn", "Vĩnh Long", "Vũng Liêm"],
        ["Bình Xuyên", "Lập Thạch", "Phúc Yên", "Sông Lô", "Tam Đảo", "Tam Dương", "Vĩnh Tường", "Vĩnh Yên", "Yên Lạc"],
        ["Lục Yên", "Mù Cang Chải", "Nghĩa Lộ", "Trạm Tấu", "Trấn Yên", "Văn Chấn", "Văn Yên", "Yên Bái", "Yên Bình"],
        ["Cẩm Lệ", "Hải Châu", "Liên Chiểu", "Ngũ Hành Sơn", "Sơn Trà", "Thanh Khê", "Hòa Vang", "Hoàng Sa Island"],
        ["Buôn Đôn", "Buôn Hồ", "Buôn Ma Thuột", "Cư M\'gar", "Cư Kuin", "Ea H\'leo", "Ea Kar", "Ea Súp", "Krông Ana", "Krông Bông", "Krông Buk", "Krông Năng", "Krông Pắk", "Lắk", "M\'Đrăk"],
        ["Cư Jút", "Đắk Glong", "Đắk Mil", "Đắk R\'Lấp", "Đắk Song", "Gia Nghĩa", "Krông Nô", "Tuy Đức"],
        ["Điện Biên", "Điện Biên Đông", "Điện Biên Phủ", "Mường Ảng", "Mường Chà", "Mường Lay", "Mường Nhé", "Nậm Pồ", "Tủa Chùa", "Tuần Giáo"],
        ["Biên Hòa", "Cẩm Mỹ", "Định Quán", "Long Khánh", "Long Thành", "Nhơn Trạch", "Tân Phú", "Thống Nhất", "Trảng Bom", "Vĩnh Cữu", "Xuân Lộc"],
        ["Cao Lãnh", "Châu Thành", "Hồng Ngự", "Lai Vung", "Lấp Vò", "Sa Đéc", "Tam Nông", "Tân Hồng", "Thanh Bình", "Tháp Mười"]
    ]
    
    @IBOutlet weak var editTitle: UILabel!
    @IBOutlet weak var nameTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var add1: UnderlineTextFieldBlack!
    @IBOutlet weak var add2: UnderlineTextFieldBlack!
    @IBOutlet weak var country: UnderlinePickerView!
    @IBOutlet weak var province: UnderlinePickerView!
    @IBOutlet weak var city: UnderlinePickerView!
    @IBOutlet weak var zipCode: UnderlineTextFieldBlack!
    @IBOutlet weak var phoneTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    weak var editDelegate: EditAddressDelegate!
    var what: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setWidgets()
    }
    
    func setWidgets() {
        if what == "ship" {
            self.editTitle.text = NSLocalizedString("ship_title", comment: "")
        } else if what == "bill" {
            self.editTitle.text = NSLocalizedString("bill_title", comment: "")
        }

        nameTextField.placeholder = NSLocalizedString("name", comment: "")
        add1.placeholder = NSLocalizedString("address1", comment: "")
        add2.placeholder = NSLocalizedString("address2", comment: "")

        country.setInit()
        country.pickerView.delegate = self
        country.pickerView.dataSource = self
        country.label.text = Country.allCases[0].localized

        province.setInit()
        province.pickerView.delegate = self
        province.pickerView.dataSource = self
        province.label.text = provinces.first
        
        city.setInit()
        city.pickerView.delegate = self
        city.pickerView.dataSource = self
        city.label.text = cities.first?.first

        zipCode.placeholder = NSLocalizedString("zipcode", comment: "")
        zipCode.keyboardType = .numberPad

        phoneTextField.placeholder = "+84 0000000000"
        phoneTextField.keyboardType = .numberPad

        submitButton.setTitle(NSLocalizedString("sign_up_submit", comment: ""), for: .normal)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case country.pickerView:
            return 1
        case province.pickerView:
            return 1
        case city.pickerView:
            return 1
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case country.pickerView:
            return 1
        case province.pickerView:
            return provinces.count
        case city.pickerView:
            return cities[province.pickerView.selectedRow(inComponent: 0)].count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case country.pickerView:
            return Country.allCases[0].localized
        case province.pickerView:
            return provinces[row]
        case city.pickerView:
            return cities[province.pickerView.selectedRow(inComponent: 0)][row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case province.pickerView:
            province.label.text = provinces[row]
            city.pickerView.selectRow(0, inComponent: 0, animated: false)
            city.label.text = cities[row][0]
        case city.pickerView:
            city.label.text = cities[province.pickerView.selectedRow(inComponent: 0)][row]
        default:
            break
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        var errorAlert: UIAlertController
        let errorOk = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        
        if !(String(nameTextField.text!).contains(" ")) {
            errorAlert = UIAlertController.init(title: "Name Error", message: "You must input first, last name.", preferredStyle: .alert)
            errorAlert.addAction(errorOk)
            self.present(errorAlert, animated: true)
            return
        }
        
        if (add1.text == ""
            || add2.text == ""
            || zipCode.text == ""
            || phoneTextField.text == "") {
            errorAlert = UIAlertController.init(title: "Error", message: "You must input all address", preferredStyle: .alert)
            errorAlert.addAction(errorOk)
            self.present(errorAlert, animated: true)
            return
        }
        
        if what == "ship" {
            editDelegate.EditShip(name: nameTextField.text!, add1: add1.text!, add2: add2.text!, country: Country.allCases[0].localized, province: provinces[province.pickerView.selectedRow(inComponent: 0)], city: cities[province.pickerView.selectedRow(inComponent: 0)][city.pickerView.selectedRow(inComponent: 0)], zip: zipCode.text!, phone: phoneTextField.text!)
        } else if what == "bill" {
            editDelegate.EditBill(name: nameTextField.text!, add1: add1.text!, add2: add2.text!, country: Country.allCases[0].localized, province: provinces[province.pickerView.selectedRow(inComponent: 0)], city: cities[province.pickerView.selectedRow(inComponent: 0)][city.pickerView.selectedRow(inComponent: 0)], zip: zipCode.text!, phone: phoneTextField.text!)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
