//
//  TutorialDelegate.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 05/05/2019.
//

import UIKit

@objc protocol TutorialDelegate: class {
    func checkTutorial()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
