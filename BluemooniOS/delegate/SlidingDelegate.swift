//
//  SlidingDelegate.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 27/04/2019.
//

import UIKit

@objc protocol SlidingDelegate: class {
    @objc optional func thisIsOptionFunc()
    func clickComingSoon()
    func clickOnSale()
    func clickCow()
    func clickCart()
    func clickWish()
    func clickMyPage()
    func clickFaq()
    func clickSettings()
    func clickPolicy()
    func clickAbout()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
