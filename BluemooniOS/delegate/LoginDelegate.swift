//
//  LoginDelegate.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 03/07/2019.
//

import UIKit

@objc protocol LoginDelegate: class {
    @objc func loginSuccess()
}
