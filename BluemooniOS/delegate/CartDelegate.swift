//
//  CartDelegate.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 30/04/2019.
//

import UIKit

@objc protocol CartDelegate: class {
    @objc func editPressed(_ cell: CartCollectionViewCell)
    @objc func deletePressed(_ cell: CartCollectionViewCell)
}
