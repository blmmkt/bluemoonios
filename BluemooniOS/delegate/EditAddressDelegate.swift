//
//  EditAddressDelegate.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 20/05/2019.
//

import UIKit

@objc protocol EditAddressDelegate: class {
    func EditShip(name: String, add1: String, add2: String, country: String, province: String, city: String, zip: String, phone: String)
    func EditBill(name: String, add1: String, add2: String, country: String, province: String, city: String, zip: String, phone: String)
}
