//
//  BankDoneViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 18/09/2019.
//

import UIKit

class BankDoneViewController: UIViewController {

    @IBOutlet weak var bankDoneTitle: UILabel!
    @IBOutlet weak var bankDoneAccount: UILabel!
    @IBOutlet weak var bankDoneTotal: UILabel!
    @IBOutlet weak var bankDoneAlert: UILabel!
    @IBOutlet weak var bankDoneConfirm: UIButton!
    
    var total: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabels()
    }
    
    func setLabels() {
        bankDoneTitle.text = NSLocalizedString("bank_done_title", comment: "")
        bankDoneAccount.text = NSLocalizedString("bank_done_account", comment: "")
        bankDoneTotal.text = "Total : " + total
        bankDoneAlert.text = NSLocalizedString("bank_done_alert", comment: "")
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindBankDoneToMain", sender: self)
    }
}
