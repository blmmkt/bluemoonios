//
//  SettingsViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 04/05/2019.
//

import UIKit
import FBSDKLoginKit
import Toaster

class SettingsViewController: UIViewController {

    @IBOutlet weak var settingsScroll: UIScrollView!
    @IBOutlet weak var settingsMyPage: UIImageView!
    @IBOutlet weak var myPageLabel: UILabel!
    @IBOutlet weak var settingsCoupon: UIImageView!
    @IBOutlet weak var couponLabel: UILabel!
    @IBOutlet weak var settingsMyOrder: UIImageView!
    @IBOutlet weak var myOrderLabel: UILabel!
    @IBOutlet weak var settingsTerms: UIImageView!
    @IBOutlet weak var termConLabel: UILabel!
    @IBOutlet weak var settingsPrivacy: UIImageView!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var settingsSignOut: UIButton!
    @IBOutlet weak var settingsOffice: OfficeInformationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setWidgets()
    }
    
    func setWidgets() {
//        settingsSignOut.setBasicInit()
        settingsSignOut.setTitle(NSLocalizedString("log_out", comment: ""), for: .normal)
        settingsSignOut.addTarget(self, action: #selector(signOut), for: .touchUpInside)
        myPageLabel.text = "My Page"
        couponLabel.text = NSLocalizedString("setting_coupon_title", comment: "")
        myOrderLabel.text = NSLocalizedString("my_order_title", comment: "")
        termConLabel.text = NSLocalizedString("setting_term_con_title", comment: "")
        privacyLabel.text = NSLocalizedString("policy_title", comment: "")
        
        settingsMyPage.isUserInteractionEnabled = true
        settingsCoupon.isUserInteractionEnabled = true
        settingsMyOrder.isUserInteractionEnabled = true
        settingsTerms.isUserInteractionEnabled = true
        settingsPrivacy.isUserInteractionEnabled = true
        
        settingsMyPage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(myPageAction)))
        settingsCoupon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(couponAction)))
        settingsMyOrder.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(myOrderAction)))
        settingsTerms.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(termConAction)))
        settingsPrivacy.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(privacyAction)))
//        settingsTutorial.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tutorialAction)))
    }
    
    @objc func signOut() {
        let alert = UIAlertController.init(title: NSLocalizedString("log_out_title", comment: ""), message: NSLocalizedString("log_out_content", comment: ""), preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: NSLocalizedString("sign_up_cancel", comment: ""), style: .cancel, handler: nil)
        let signOutAction = UIAlertAction.init(title: NSLocalizedString("log_out", comment: ""), style: .destructive) { (UIAlertAction) in
            Constants.USER_DEFAULT.set(false, forKey: "login")
            
            if Constants.USER_DEFAULT.bool(forKey: "fb_login") {
                Constants.USER_DEFAULT.setValue(false, forKey: "fb_login")
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
            }
            Constants.USER_DEFAULT.setValue(nil, forKey: "id")
            Constants.USER_DEFAULT.setValue(nil, forKey: "pw")
            Constants.USER_DEFAULT.setValue(nil, forKey: "tkn")
            
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(signOutAction)
        present(alert, animated: true)
    }
    
    @objc func myPageAction() {
        self.performSegue(withIdentifier: "unwindSettingsToMain", sender: self)
        self.performSegue(withIdentifier: "settingToMyPageSegue", sender: self)
    }
    
    @objc func couponAction() {
        Toast(text: "Coming Soon!", delay: 0, duration: 2.0).show()
    }
    
    @objc func myOrderAction() {
        let orderVC = OrderListViewController()
        navigationController?.pushViewController(orderVC, animated: true)
    }
    
    @objc func termConAction() {
        guard let path = Bundle.main.path(forResource: "market_policy_en", ofType: "txt") else { return  }
        do {
            let content = try String(contentsOfFile: path, encoding: .utf8)
            
            let alert = UIAlertController(title: NSLocalizedString("setting_term_con_dialog_title", comment: ""), message: content, preferredStyle: .alert)
            let dismissAlert = UIAlertAction(title: NSLocalizedString("complete_positive", comment: ""), style: .cancel, handler: nil)
            alert.addAction(dismissAlert)
            present(alert, animated: true, completion: nil)
        } catch {
            
        }
    }
    
    @objc func privacyAction() {
        guard let path = Bundle.main.path(forResource: "policy_en", ofType: "txt") else { return  }
        do {
            let content = try String(contentsOfFile: path, encoding: .utf8)
            
            let alert = UIAlertController(title: NSLocalizedString("policy_title", comment: ""), message: content, preferredStyle: .alert)
            let dismissAlert = UIAlertAction(title: NSLocalizedString("complete_positive", comment: ""), style: .cancel, handler: nil)
            alert.addAction(dismissAlert)
            present(alert, animated: true, completion: nil)
        } catch {
            
        }
    }
    
//    @objc func tutorialAction() {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "tutorialStory") as! TutorialViewController
//        present(vc, animated: true)
//    }
}
