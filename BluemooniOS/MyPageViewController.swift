//
//  MyPageViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class MyPageViewController: UIViewController, UITabBarDelegate {
    
    @IBOutlet weak var myPageScroll: UIScrollView!
    @IBOutlet weak var personalTitle: UILabel!
    @IBOutlet weak var personalWrapper: UIView!
    @IBOutlet weak var personalThumb: CustomCircleImageView!
    @IBOutlet weak var personalName: CustomDrawableTextField!
    @IBOutlet weak var personalMail: CustomDrawableTextField!
    @IBOutlet weak var personalBirth: CustomDrawableTextField!
    @IBOutlet weak var personalPhone: CustomDrawableTextField!
    @IBOutlet weak var personalRank: CustomDrawableTextField!
    @IBOutlet weak var personalPoint: CustomDrawableTextField!
    @IBOutlet weak var addressWrapper: UIView!
    @IBOutlet weak var addressTitle: UILabel!
    @IBOutlet weak var addressAddress: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var myPageOffice: OfficeInformationView!
    @IBOutlet weak var myPageBottomNav: UITabBar!
    @IBOutlet weak var editProfileButton: UIButton!
    
//    var textViewInAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setWidgets()
        getMyInformation()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLayoutSubviews() {
        myPageScroll.contentInset.bottom = myPageOffice.frame.origin.x + 49
    }
    
    func setWidgets() {
        personalTitle.text = NSLocalizedString("info_title", comment: "")
        personalWrapper.layer.cornerRadius = 15
        personalName.setInit(margin: 144)
        personalName.setImageSrc(imageName: "icon_profile")
        personalName.setEditable(false)
        personalMail.setInit(margin: 144)
        personalMail.setImageSrc(imageName: "icon_mail")
        personalMail.setEditable(false)
        personalBirth.setInit(margin: 144)
        personalBirth.setImageSrc(imageName: "icon_birth")
        personalBirth.setEditable(false)
        personalPhone.setInit(margin: 144)
        personalPhone.setImageSrc(imageName: "icon_phone")
        personalPhone.setEditable(false)
        personalRank.setInit(margin: 144)
        personalRank.setImageSrc(imageName: "icon_rank")
        personalRank.setEditable(false)
        personalPoint.setInit(margin: 144)
        personalPoint.setImageSrc(imageName: "icon_point")
        personalPoint.setEditable(false)
        addressWrapper.layer.cornerRadius = 15
        addressTitle.text = NSLocalizedString("address_title", comment: "")
//        editProfileButton.setBasicInit()
        editProfileButton.setTitle(NSLocalizedString("edit_mine", comment: ""), for: .normal)
        
        setAddressAdress()
        
        myPageBottomNav.delegate = self
    }
    
    func setAddressAdress() {
//        addressAddress.layer.borderColor = UIColor.white.cgColor
//        addressAddress.layer.borderWidth = 3
//        addressAddress.layer.cornerRadius = 24
        
//        let innerView = UIView()
//        innerView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: addressAddress.frame.height)
//        innerView.layer.borderWidth = 1
//        innerView.layer.borderColor = UIColor.white.cgColor
//        innerView.layer.cornerRadius = 19
//        addressAddress.addSubview(innerView)
        
//        let imageView = UIImageView()
//        imageView.image = UIImage(named: "icon_home")
//        imageView.frame = CGRect.init(x: 10, y: 10, width: 25, height: 25)
//        addressAddress.addSubview(imageView)
//
//        textViewInAddress = UILabel()
//        textViewInAddress.frame = CGRect.init(x: 35, y: 10, width: addressAddress.frame.width - 35, height: 50)
//        textViewInAddress.numberOfLines = 5
//        textViewInAddress.textColor = .black
//        textViewInAddress.textAlignment = .natural
//        addressAddress.addSubview(textViewInAddress)
    }
    
    func getMyInformation() {
        Alamofire.request(
            Constants.BASE_URL + "User_Info_Ok.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? ""],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResUserInfo>) in
                if let result = response.result.value {
                    if(result.error_msg == "") {
                        let processor = RoundCornerImageProcessor(cornerRadius: 43)
                        
                        if result.first_name == "" {
                            self.personalName.textField.text = "Guest"
                            self.editProfileButton.isHidden = true
                        } else {
                            self.personalThumb.realImage.kf.setImage(with: URL(string: String(result.thumbnail)), placeholder: nil, options: [.processor(processor)])
                            
                            self.personalName.textField.text = result.first_name + " " + result.last_name
                            self.personalMail.textField.text = result.email
                            self.personalBirth.textField.text = UtilMethod.getLocalBirthDay(birth: result.birthday)
                            self.personalPhone.textField.text = result.phone_num
                            self.personalRank.textField.text = String(result.rank)
                            self.personalPoint.textField.text = String(result.point)
                            
                            self.addressLabel.text = result.address1 + ", " + result.city + ", " + result.province + ", " + result.country + ", " + "\r\n\r\n" + result.address2
                            
                            self.view.layoutIfNeeded()
                        }
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            break
        case 1:
            self.performSegue(withIdentifier: "unwindMyPageToMain", sender: self)
            self.performSegue(withIdentifier: "myPageToCart", sender: self)
            break
        default:
            break
        }
    }
    
    @IBAction func editProfileAction(_ sender: CustomButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "signUpStory") as! SignUpViewController
        vc.isEdit = true
        
        self.present(vc, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
