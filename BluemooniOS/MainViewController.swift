//
//  MainViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 19/04/2019.
//

import UIKit
import NavigationDrawer
import Alamofire
import AlamofireObjectMapper
import Toaster
import Firebase

class MainViewController: /*UIViewController, */VersionCheckViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITabBarDelegate, SlidingDelegate {
    
    let interactor = Interactor()
    @IBOutlet weak var homeScroll: UIScrollView!
    @IBOutlet weak var comingSoonBack: UIView!
    @IBOutlet weak var comingSoonTitle: UILabel!
    @IBOutlet weak var mainToComingSoon: UIButton!
    @IBOutlet weak var teaseCollection: UICollectionView!
    @IBOutlet weak var mainToSale: UIButton!
    @IBOutlet weak var saleBack: UIView!
    @IBOutlet weak var saleTitle: UILabel!
    @IBOutlet weak var saleCollection: UICollectionView!
    @IBOutlet weak var officeInfo: OfficeInformationView!
    @IBOutlet weak var bottomNav: UITabBar!
    
    var teaseDataList: Array<ResTease.Tease>! = []
    var salesDataList: Array<ResSales.Sale>! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setWidgets()
    }
    
    func setWidgets() {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)

        let logo = UIImage(named: "bluemoon_ci")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect.init(x: 0, y: 0, width: 30, height: 20)
        navigationItem.titleView = imageView
        
        bottomNav.delegate = self
        
        mainToSale.setTitle(NSLocalizedString("show_more", comment: ""), for: .normal)
        setSaleCollectionView()
        getSalesData()
        
        mainToComingSoon.setTitle(NSLocalizedString("show_more", comment: ""), for: .normal)
        setTeaseCollectionView()
        getTeaseData()
    }
    
    override func viewDidLayoutSubviews() {
        homeScroll.contentSize = CGSize.init(width: UIScreen.main.bounds.width, height: officeInfo.frame.origin.y + officeInfo.bounds.height + 56)
    }
    
    func setTeaseCollectionView() {
        teaseCollection.delegate = self
        teaseCollection.dataSource = self
    }
    
    func getTeaseData() {
        Alamofire.request(
            Constants.BASE_URL + "Product_Tease_List.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "page": 0],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResTease>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.teaseDataList = result.sales_list
                        
                        self.teaseCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func setSaleCollectionView() {
        saleCollection.delegate = self
        saleCollection.dataSource = self
    }
    
    func getSalesData() {
        Alamofire.request(
            Constants.BASE_URL + "Product_Sale_List.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "page": 0],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResSales>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.salesDataList = result.sales_list
                        
                        self.saleCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == teaseCollection) {
            return self.teaseDataList.count == 0 ? 1 : self.teaseDataList.count
        } else {
            return self.salesDataList.count == 0 ? 1 : self.salesDataList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == teaseCollection) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeTeaseCell", for: indexPath) as! HomeTeaseCollectionViewCell
        
            if teaseDataList.count == 0 {
                cell.imageView.image = UIImage(named: "icon_empty")
                cell.progress.isHidden = true
                cell.salesPercent.isHidden = true
                cell.likeImg.isHidden = true
                cell.timerText.isHidden = true
                cell.brand.text = NSLocalizedString("no_item", comment: "")
                cell.product.isHidden = true
//                cell.originPrice.isHidden = true
//                cell.discountRate.isHidden = true
//                cell.price.isHidden = true
                
                return cell
            } else {
                cell.progress.isHidden = false
                cell.salesPercent.isHidden = false
                cell.likeImg.isHidden = false
                cell.timerText.isHidden = false
                cell.product.isHidden = false
//                cell.price.isHidden = false
//                cell.originPrice.isHidden = true
//                cell.discountRate.isHidden = true
//                cell.price.isHidden = true
                
                let teaseData = self.teaseDataList[indexPath.row]

                let oriUrl = teaseData.imgPath + teaseData.thumbnails.split(separator: "|")[0]
                let kor = oriUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let urlKor = URL(string: kor)

                cell.imageView.kf.setImage(with: urlKor)
                
                cell.progress.setSecondaryProgress(progress: Float(teaseData.minimum / teaseData.maximum))
    //            cell.progress.progress = Float(teaseData.sales / teaseData.maximum)
                cell.progress.setProgress(Float(teaseData.sales / teaseData.maximum), animated: true)
//                cell.salesPercent.text = String(Int(Float(teaseData.sales / teaseData.minimum) * 100)) + "%"
                
                cell.salesPercent.text = String.init(format: "%.0f", teaseData.maximum) + " left"
                
                if teaseData.sales / teaseData.minimum > 1 {
                    cell.salesPercent.textColor = UIColor.red
                }
                
                cell.likeImg.tag = indexPath.row + 1
                
                if teaseData.my_like {
                    cell.likeImg.setImage(UIImage(named: "icon_wish"), for: .normal)

                    cell.likeImg.tag *= -1
                } else {
                    cell.likeImg.setImage(UIImage(named: "icon_wish_empty"), for: .normal)
                }
                
                cell.brand.text = teaseData.manufacturer
                cell.product.text = teaseData.name
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                if let date = dateFormatter.date(from: teaseData.sale_start) {
                    cell.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                        cell.timerText.text = date.countDownFormat
                        if Date() > date {
                            timer.invalidate()
                        }
                    })
                    cell.timer?.fire()
                    RunLoop.current.add(cell.timer!, forMode: .common)
                }
            
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeSaleCell", for: indexPath) as! HomeSaleCollectionViewCell
            
            if salesDataList.count == 0 {
                cell.imageView.image = UIImage(named: "icon_empty")
                cell.progress.isHidden = true
                cell.salesPercent.isHidden = true
                cell.likeImg.isHidden = true
                cell.timerText.isHidden = true
                cell.brand.text = NSLocalizedString("no_item", comment: "")
                cell.product.isHidden = true
                cell.originPrice.isHidden = true
                cell.discountRate.isHidden = true
                cell.price.isHidden = true
                
                return cell
            } else {
                cell.progress.isHidden = false
                cell.salesPercent.isHidden = false
                cell.likeImg.isHidden = false
                cell.timerText.isHidden = false
                cell.product.isHidden = false
                cell.price.isHidden = false
                
                let salesData = self.salesDataList[indexPath.row]
                
                let oriUrl = salesData.imgPath + salesData.thumbnails.split(separator: "|")[0]
                let kor = oriUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let urlKor = URL(string: kor)
                
                cell.imageView.kf.setImage(with: urlKor)
                
                cell.progress.setSecondaryProgress(progress: Float(salesData.minimum / salesData.maximum))
    //            cell.progress.progress = Float(salesData.sales / salesData.maximum)
                cell.progress.setProgress(Float(salesData.sales / salesData.maximum), animated: true)
                
                cell.likeImg.tag = indexPath.row + 1
                
                if salesData.my_like {
                    cell.likeImg.setImage(UIImage(named: "icon_wish"), for: .normal)
                    
                    cell.likeImg.tag *= -1
                } else {
                    cell.likeImg.setImage(UIImage(named: "icon_wish_empty"), for: .normal)
                }
                
//                cell.salesPercent.text = String(Int(Float(salesData.sales / salesData.minimum) * 100)) + "%"
                
                cell.salesPercent.text = String.init(format: "%.0f", salesData.maximum) + " left"
                
                if salesData.sales / salesData.minimum > 1 {
                    cell.salesPercent.textColor = UIColor.red
                }
                
                cell.brand.text = salesData.manufacturer
                cell.product.text = salesData.name
                
                if(salesData.discount == "Y") {
                    if salesData.discount_rate > 0 {
                        cell.setOriginPrice(text: UtilMethod.getForattedPrice(price: (UtilMethod.getOriginPrice(priceString: salesData.price, discount_rate: salesData.discount_rate))) + " " + UtilMethod.getLocalUnit())
                        
                        cell.price.text = UtilMethod.getForattedPrice(price: UtilMethod.getLocalPrice(priceString: salesData.price)) + " " + UtilMethod.getLocalUnit()
                        cell.discountRate.text = String(salesData.discount_rate) + " %"
                        cell.originPrice.isHidden = false
                        cell.discountRate.isHidden = false
                    } else {
                        cell.originPrice.isHidden = true
                        cell.discountRate.isHidden = true
                    }
                } else {
                    cell.price.text = UtilMethod.getForattedPrice(price: (UtilMethod.getLocalPrice(priceString: salesData.price))) + " " + UtilMethod.getLocalUnit()
                    cell.originPrice.isHidden = true
                    cell.discountRate.isHidden = true
                }
                
                var date: Date?
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                date = dateFormatter.date(from: salesData.sale_end)
                
                if let date = date {
                    cell.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                        cell.timerText.text = date.countDownFormat
                        if Date() > date {
                            timer.invalidate()
                        }
                    })
                    cell.timer?.fire()
                    RunLoop.current.add(cell.timer!, forMode: .common)
                }
                
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let orderVC = OrderListViewController()
//        navigationController?.pushViewController(orderVC, animated: true)
//        return
        
        if(collectionView == teaseCollection) {
            if teaseDataList.count > 0 {
                let tease = teaseDataList[indexPath.row]
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "detailComingSoonViewController") as! DetailComingSoonViewController
                vc.product = tease
                navigationController?.pushViewController(vc, animated: true)
                
                Analytics.logEvent("DetailView", parameters: [
                    "mainToDetail": "comingsoon" as NSObject
                    ])
            }
        } else {
            if salesDataList.count > 0 {
                let sale = salesDataList[indexPath.row]
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "detailComingSoonViewController") as! DetailComingSoonViewController
                vc.product = sale
                navigationController?.pushViewController(vc, animated: true)
                
                Analytics.logEvent("DetailView", parameters: [
                    "mainToDetail": "sale" as NSObject
                    ])
            }
        }
    }
    
    @IBAction func teaseLikeImgAction(_ sender: UIButton) {
        if sender.tag < 0 {
            teaseDislike(position: (-1) * (sender.tag + 1))
        } else {
            teaseLike(position: (sender.tag - 1))
        }
    }
    
    @IBAction func saleLikeImgAction(_ sender: UIButton) {
        if sender.tag < 0 {
            saleDislike(position: (-1) * (sender.tag + 1))
        } else {
            saleLike(position: (sender.tag - 1))
        }
    }
    
    func teaseLike(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Ok.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(teaseDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.teaseDataList[position].my_like = true
                        
                        self.teaseCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func teaseDislike(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Cancle.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(teaseDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.teaseDataList[position].my_like = false
                        
                        self.teaseCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    @objc func saleLike(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Ok.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(salesDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.salesDataList[position].my_like = true
                        
                        self.saleCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    @objc func saleDislike(position: Int) {
        Alamofire.request(
            Constants.BASE_URL + "Product_Like_Cancle.jsp",
            method: .post,
            parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? "",
                         "item_id": String(salesDataList[position].id)],
            encoding: URLEncoding.default,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseObject {(response: DataResponse<ResFind>) in
                if let result = response.result.value {
                    if(result.error_msg == "ok") {
                        self.salesDataList[position].my_like = false
                        
                        self.saleCollection.reloadData()
                    } else {
                        print(result.error_msg)
                    }
                }
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            print("1")
            break
        case 1:
            clickCart()
            self.bottomNav.selectedItem = nil
            break
        case 2:
            clickMyPage()
            self.bottomNav.selectedItem = nil
            break
        default:
            break
        }
    }
    
    @IBAction func homeButtonPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "showSlidingMenu", sender: nil)
    }
    
    @IBAction func edgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .Right)
        
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor){
                self.performSegue(withIdentifier: "showSlidingMenu", sender: nil)
        }
    }
    //3. Add a Pan Gesture to slide the menu from Certain Direction
    
    //4. Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? SlidingViewController {
            destinationViewController.mainDelegate = self
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = self.interactor
        }
        
        if let destinationViewController = segue.destination as? SignInViewController {
            destinationViewController.delegate = self
        }
    }
    
    func clickComingSoon() {
        self.performSegue(withIdentifier: "comingsoonSegue", sender: self)
    }
    
    func clickOnSale() {
        self.performSegue(withIdentifier: "saleSegue", sender: self)
    }
    
    func clickCow() {
        self.performSegue(withIdentifier: "cowSegue", sender: self)
    }
    
    func clickCart() {
        self.performSegue(withIdentifier: "cartSegue", sender: self)
    }
    
    func clickWish() {
//        self.performSegue(withIdentifier: "comingsoonSegue", sender: self)
        Toast(text: "Coming Soon!", delay: 0, duration: 2.0).show()
    }
    
    func clickMyPage() {
        if (Constants.USER_DEFAULT.string(forKey:"id")?.contains("guest"))! {
            self.performSegue(withIdentifier: "guestLogin", sender: self)
        } else {
            self.performSegue(withIdentifier: "myPageSegue", sender: self)
        }
    }
    
    func clickFaq() {
        Toast(text: "Coming Soon!", delay: 0, duration: 2.0).show()
    }
    
    func clickSettings() {
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    func clickPolicy() {
        guard let path = Bundle.main.path(forResource: "policy_en", ofType: "txt") else { return  }
        do {
            let content = try String(contentsOfFile: path, encoding: .utf8)
            
            let alert = UIAlertController(title: NSLocalizedString("policy_title", comment: ""), message: content, preferredStyle: .alert)
            let dismissAlert = UIAlertAction(title: NSLocalizedString("complete_positive", comment: ""), style: .cancel, handler: nil)
            alert.addAction(dismissAlert)
            present(alert, animated: true, completion: nil)
        } catch {
            
        }
    }
    
    func clickAbout() {
        let aboutVC = AboutCompanyViewController()
        present(aboutVC, animated: true, completion: nil)
    }
    
    @IBAction func unwindToMain(_ unwindSegue: UIStoryboardSegue) {
        
    }

    @IBAction func mainToComingSoonAction(_ sender: UIButton) {
        Analytics.logEvent("ShowMore", parameters: [
            "showmore": "comingsoon" as NSObject
            ])
    }
    
    @IBAction func mainToSaleAction(_ sender: UIButton) {
        Analytics.logEvent("ShowMore", parameters: [
            "showmore": "sale" as NSObject
            ])
    }
}

extension MainViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

extension MainViewController: LoginDelegate {
    func loginSuccess() {
        
    }
}
