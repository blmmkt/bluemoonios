//
//  SignUpViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 28/04/2019.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import FacebookCore
import FBSDKLoginKit
import Firebase

enum Country: Int, CaseIterable {
    case Vietnam = 84
    var localized: String {
        return "Việt Nam"
    }
}

class SignUpViewController: UIViewController {
    
    let countryCodes: [String] = ["+84"]
    let provinces: [String] = [
        "An Giang", "Bà Rịa-Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Dương", "Bình Phước", "Bình Thuận", "Bình Định", "Cà Mau", "Cần Thơ", "Cao Bằng", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Nội", "Hà Tĩnh", "Hải Dương", "Hải Phòng", "Hậu Giang", "Hồ Chí Minh", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú yên", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên-Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Đà Nẵng", "Đắk Lắk", "Đắk Nông", "Diễn Biến", "Đồng Nai", "Đồng Tháp"
    ]
    let cities: [[String]] = [
        ["An Phú", "Châu Đốc", "Châu Phú", "Châu Thành", "Chợ Mới", "Long Xuyên", "Phú Tân", "Tân Châu", "Thoại Sơn", "Tịnh Biên", "Tri Tôn"],
        ["Bà Rịa", "Châu Đức", "Côn Đảo", "Đặt Đồ", "Long Điền", "Tân Thành", "Vũng Tàu", "Xuyên Mộc"],
        ["Bắc Giang", "Hiệp Hòa", "Lạng Giang", "Lục Nam", "Lục Ngạn", "Sơn Đông", "Tân Yên", "Việt Yên", "Yên Dũng", "Yên Thế"],
        ["Ba Bể", "Bạch Thông", "Bắc Kạn", "Chợ Đồn", "Chợ Mới", "Na Rì", "Ngân Sơn", "Pác Nặm"],
        ["Bạc Liêu", "Giá Rai", "Đông Hải", "Hòa Binh", "Hồng Dân", "Phước Long", "Vĩnh Lợi"],
        ["Bắc Ninh", "Gia Bình", "Lương Tài", "Quế Võ", "Thuận Thành", "Tiên Du", "Từ Sơn", "Yên Phong"],
        ["Ba Tri", "Bến Tre", "Bình Đại", "Châu Thành", "Chợ Lách", "Giồng Trôm", "Mỏ Cày Bắc", "Mỏ Cày Nam", "Thạnh Phú"],
        ["Bến Cát", "Dầu Tiếng", "Dĩ An", "Phú Giáo", "Tân Uyên", "Thủ Dầu Một", "Thuận An"],
        ["Bình Long", "Bù Đăng", "Bù Đốp", "Chơn Thành", "Đồng Phú", "Đồng Xoài", "Lộc Ninh", "Phước Long"],
        ["Bắc Bình", "Đức Linh", "Hàm Tân", "Hàm Thuận Bắc", "Hàm Thuận Nam", "La Gi", "Phan Thiết", "Phú Quý", "Tánh Linh", "Tuy Phong"],
        ["An Lão", "An Nhơn", "Hoài Ân", "Hoài Nhơn", "Phù Cát", "Phù Mỹ", "Quy Nhơn", "Tây Sơn", "Tuy Phước", "Vân Canh", "Vĩnh Thạnh"],
        ["Cà Mau", "Cái Nước", "Đầm Dơi", "Năm Căn", "Ngọc Hiển", "Phú Tân", "Thới Bình", "Trần Văn Thời", "U Minh"],
        ["Bình Thủy", "Cái Răng", "Cờ Đỏ", "Ninh Kiều", "Ô Môn", "Phong Điền", "Thốt Nốt", "Vĩnh Thạnh"],
        ["Bảo Lạc", "Bảo Lâm", "Cao Bằng", "Hà Quảng", "Hạ Lang", "Hòa An", "Nguyên Bình", "Phục Hòa", "Quảng Uyên", "Thạch An", "Thông Nông", "Trà Lĩnh", "Trùng Khánh"],
        ["An Khê", "Ayun Pa", "Chư Păh", "Chư Prông", "Chư Sê", "Đắk Đoa", "Đắk Pơ", "Đức Cơ", "Ia Grai", "Ia Pa", "K\'Bang", "Kông Chro", "Krông Pa", "Mang Yang", "Phú Thiện", "Pleiku"],
        ["Bắc Mê", "Bắc Quang", "Đồng Văn", "Hà Giang", "Hoàng Su Phì", "Mèo Vạc", "Quản Bạ", "Quảng Bình", "Vị Xuyên", "Xín Mần", "Yên Minh"],
        ["Bình Lục", "Duy Tiên", "Kim Bảng", "Lý Nhân", "Phủ Lý", "Thanh Liêm"],
        ["Ba Đình", "Cầu Giấy", "Đông Anh", "Đống Đa", "Gia Lâm", "Hai Bà Trưng", "Hoàn Kiếm", "Hoàng Mai", "Long Biên", "Sóc Sơn", "Tây Hồ", "Thanh Trì", "Thanh Xuân", "Từ Liêm", "Ba Vì", "Chương Mỹ", "Đan Phượng", "Hà Đông", "Hoài Đức", "Mỹ Đức", "Phú Xuyên", "Phúc Thọ", "Quốc Oai", "Sơn Tây", "Thạch Thất", "Thanh Oai", "Thường Tín", "Ứng Hòa", "Mê Linh"],
        ["Can Lộc", "Cẩm Xuyên", "Đức Thọ", "Hà Tĩnh", "Hồng Lĩnh", "Hương Khê", "Hương Sơn", "Kỳ Anh", "Lộc Hà", "Nghi Xuân", "Thạch Hà", "Vũ Quang"],
        ["Bình Giang", "Cẩm Giàng", "Chí Linh", "Gia Lộc", "Hải Dương", "Kim Thành", "Kinh Môn", "Nam Sách", "Ninh Giang", "Thanh Hà", "Thanh Miện", "Tứ Kỳ"],
        ["An Dương", "An Lão", "Bạch Long Vĩ", "Cát Hải", "Dương Kinh", "Đồ Sơn", "Hồng Bàng", "Kiến An", "Kiến Thụy", "Lê Chân", "Ngô Quyền", "Thủy Nguyên", "Tiên Lãng", "Vĩnh Bảo", "Hải An"],
        ["Châu Thành", "Châu Thành A", "Long Mỹ", "Phụng Hiệp", "Ngã Bảy (formerly Tân Hiệp)", "Vị Thanh", "Vị Thủy"],
        ["Bình Chánh", "Bình Tân", "Bình Thạnh", "Cần Giờ", "Củ Chi", "Gò Vấp", "Hóc Môn", "Nhà Bè", "Phú Nhuận", "Quận 1", "Quận 2", "Quận 3", "Quận 4", "Quận 5", "Quận 6", "Quận 7", "Quận 8", "Quận 9", "Quận 10", "Quận 11", "Quận 12", "Tân Bình", "Tân Phú", "Thủ Đức"],
        ["Cao Phong", "Đà Bắc", "Hòa Bình", "Kim Bôi", "Kỳ Sơn", "Lạc Sơn", "Lạc Thủy", "Lương Sơn", "Mai Châu", "Tân Lạc", "Yên Thủy"],
        ["Ân Thi", "Hưng Yên", "Khoái Châu", "Kim Động", "Mỹ Hào", "Phù Cừ", "Tiên Lữ", "Văn Giang", "Văn Lâm", "Yên Mỹ"],
        ["Cam Lâm", "Cam Ranh", "Diên Khánh", "Khánh Sơn", "Khánh Vĩnh", "Nha Trang", "Ninh Hòa", "Trường Sa", "Vạn Ninh"],
        ["An Biên", "An Minh", "Châu Thành", "Giồng Riềng", "Gò Quao", "Hà Tiên", "Hòn Đất", "Kiên Hải", "Kiên Lương", "Phú Quốc", "Rạch Giá", "Tân Hiệp", "U Minh Thượng", "Vĩnh Thuận"],
        ["Đắk Glei", "Đắk Hà", "Đắk Tô", "Kon Plông", "Kon Rẫy", "Kon Tum", "Ngọc Hồi", "Sa Thầy", "Tu Mơ Rông"],
        ["Lai Châu", "Mường Tè", "Phong Thổ", "Sìn Hồ", "Tam Đường", "Than Uyên"],
        ["Bảo Lâm", "Bảo Lộc", "Cát Tiên", "Di Linh", "Da Lat", "Đạ Huoai", "Đạ Tẻh", "Đam Rông", "Đơn Dương", "Đức Trọng", "Lạc Dương", "Lâm Hà"],
        ["Bắc Sơn", "Bình Gia", "Cao Lộc", "Chi Lăng", "Đình Lập", "Hữu Lũng", "Lạng Sơn", "Lộc Bình", "Tràng Định", "Văn Lãng", "Văn Quan"],
        ["Bảo Thắng", "Bảo Yên", "Bát Xát", "Bắc Hà", "Lào Cai", "Mường Khương", "Sa Pa", "Si Ma Cai", "Văn Bàn"],
        ["Bến Lức", "Cần Đước", "Cần Giuộc", "Châu Thành", "Đức Hòa", "Đức Huệ", "Mộc Hóa", "Tân An", "Tân Hưng", "Tân Thạnh", "Tân Trụ", "Thạnh Hóa", "Thủ Thừa", "Vĩnh Hưng"],
        ["Giao Thủy", "Hải Hậu", "Mỹ Lộc", "Nam Định", "Nam Trực", "Nghĩa Hưng", "Trực Ninh", "Vụ Bản", "Xuân Trường", "Ý Yên"],
        ["Anh Sơn", "Con Cuông", "Cửa Lò", "Diễn Châu", "Đô Lương", "Hưng Nguyên", "Kỳ Sơn", "Nam Đàn", "Nghi Lộc", "Nghĩa Đàn", "Quế Phong", "Quỳ Châu", "Quỳ Hợp", "Quỳnh Lưu", "Tân Kỳ", "Thanh Chương", "Tương Dương", "Vinh", "Yên Thành"],
        ["Gia Viễn", "Hoa Lư", "Kim Sơn", "Nho Quan", "Ninh Bình", "Tam Điệp", "Yên Khánh", "Yên Mô"],
        ["Bác Ái", "Ninh Hải", "Ninh Phước", "Ninh Sơn", "Phan Rang–Tháp Chàm", "Thuận Bắc", "Thuận Nam"],
        ["Cẩm Khê", "Đoan Hùng", "Hạ Hòa", "Lâm Thao", "Phú Thọ", "Phù Ninh", "Tam Nông", "Tân Sơn", "Thanh Ba", "Thanh Sơn", "Thanh Thủy", "Việt Trì", "Yên Lập"],
        ["Đông Hòa", "Đồng Xuân", "Phú Hòa", "Sông Cầu", "Sông Hinh", "Sơn Hòa", "Tây Hòa", "Tuy An", "Tuy Hòa"],
        ["Bố Trạch", "Đồng Hới", "Lệ Thủy", "Minh Hóa", "Quảng Ninh", "Quảng Trạch", "Tuyên Hóa"],
        ["Bắc Trà My", "Duy Xuyên", "Đại Lộc", "Điện Bàn", "Đông Giang", "Hiệp Đức", "Hội An", "Nam Giang", "Nam Trà My", "Núi Thành", "Phú Ninh", "Phước Sơn", "Quế Sơn", "Tam Kỳ", "Tây Giang", "Thăng Bình", "Tiên Phước", "Nông Sơn"],
        ["Ba Tơ", "Bình Sơn", "Đức Phổ", "Lý Sơn", "Minh Long", "Mộ Đức", "Nghĩa Hành", "Sơn Hà", "Sơn Tây", "Sơn Tịnh", "Tây Trà", "Trà Bồng", "Tư Nghĩa", "Quảng Ngãi"],
        ["Ba Chẽ", "Bình Liêu", "Cẩm Phả", "Cô Tô", "Đầm Hà", "Đông Triều", "Hạ Long", "Hải Hà", "Hoành Bồ", "Móng Cái", "Tiên Yên", "Uông Bí", "Vân Đồn", "Quảng Yên"],
        ["Cam Lộ", "Cồn Cỏ", "Đa Krông", "Đông Hà", "Gio Linh", "Hải Lăng", "Hướng Hóa", "Quảng Trị", "Triệu Phong", "Vĩnh Linh"],
        ["Châu Thành", "Cù Lao Dung", "Kế Sách", "Long Phú", "Mỹ Tú", "Mỹ Xuyên", "Ngã Năm", "Sóc Trăng", "Thạnh Trị", "Trần Đề", "Vĩnh Châu"],
        ["Bắc Yên", "Mai Sơn", "Mộc Châu", "Mường La", "Phù Yên", "Quỳnh Nhai", "Sông Mã", "Sốp Cộp", "Sơn La", "Thuận Châu", "Yên Châu"],
        ["Bến Cầu", "Châu Thành", "Dương Minh Châu", "Gò Dầu", "Hòa Thành", "Tân Biên", "Tân Châu", "Tây Ninh", "Trảng Bàng"],
        ["Đông Hưng", "Hưng Hà", "Kiến Xương", "Quỳnh Phụ", "Thái Bình", "Thái Thụy", "Tiền Hải", "Vũ Thư"],
        ["Đại Từ", "Định Hóa", "Đồng Hỷ", "Phổ Yên", "Phú Bình", "Phú Lương", "Sông Công", "Thái Nguyên", "Võ Nhai"],
        ["Bá Thước", "Bỉm Sơn", "Cẩm Thủy", "Đông Sơn", "Hà Trung", "Hậu Lộc", "Hoằng Hóa", "Lang Chánh", "Mường Lát", "Nga Sơn", "Ngọc Lặc", "Như Thanh", "Như Xuân", "Nông Cống", "Quan Hóa", "Quan Sơn", "Quảng Xương", "Sầm Sơn", "Thạch Thành", "Thanh Hóa", "Thiệu Hóa", "Thọ Xuân", "Thường Xuân", "Tĩnh Gia", "Triệu Sơn", "Vĩnh Lộc", "Yên Định"],
        ["A Lưới", "Huế", "Hương Thủy", "Hương Trà", "Nam Đông", "Phong Điền", "Phú Lộc", "Phú Vang", "Quảng Điền"],
        ["Cái Bè", "Cai Lậy", "Châu Thành", "Chợ Gạo", "Gò Công", "Gò Công Dông", "Gò Công Tây", "Mỹ Tho", "Tân Phú Đông", "Tân Phước"],
        ["Càng Long", "Cầu Kè", "Cầu Ngang", "Châu Thành", "Duyên Hải", "Tiểu Cần", "Trà Cú", "Trà Vinh"],
        ["Chiêm Hóa", "Hàm Yên", "Lâm Bình", "Nà Hang", "Sơn Dương", "Tuyên Quang", "Yên Sơn"],
        ["Bình Minh", "Bình Tân", "Long Hồ", "Mang Thít", "Tâm Bình", "Trà Ôn", "Vĩnh Long", "Vũng Liêm"],
        ["Bình Xuyên", "Lập Thạch", "Phúc Yên", "Sông Lô", "Tam Đảo", "Tam Dương", "Vĩnh Tường", "Vĩnh Yên", "Yên Lạc"],
        ["Lục Yên", "Mù Cang Chải", "Nghĩa Lộ", "Trạm Tấu", "Trấn Yên", "Văn Chấn", "Văn Yên", "Yên Bái", "Yên Bình"],
        ["Cẩm Lệ", "Hải Châu", "Liên Chiểu", "Ngũ Hành Sơn", "Sơn Trà", "Thanh Khê", "Hòa Vang", "Hoàng Sa Island"],
        ["Buôn Đôn", "Buôn Hồ", "Buôn Ma Thuột", "Cư M\'gar", "Cư Kuin", "Ea H\'leo", "Ea Kar", "Ea Súp", "Krông Ana", "Krông Bông", "Krông Buk", "Krông Năng", "Krông Pắk", "Lắk", "M\'Đrăk"],
        ["Cư Jút", "Đắk Glong", "Đắk Mil", "Đắk R\'Lấp", "Đắk Song", "Gia Nghĩa", "Krông Nô", "Tuy Đức"],
        ["Điện Biên", "Điện Biên Đông", "Điện Biên Phủ", "Mường Ảng", "Mường Chà", "Mường Lay", "Mường Nhé", "Nậm Pồ", "Tủa Chùa", "Tuần Giáo"],
        ["Biên Hòa", "Cẩm Mỹ", "Định Quán", "Long Khánh", "Long Thành", "Nhơn Trạch", "Tân Phú", "Thống Nhất", "Trảng Bom", "Vĩnh Cữu", "Xuân Lộc"],
        ["Cao Lãnh", "Châu Thành", "Hồng Ngự", "Lai Vung", "Lấp Vò", "Sa Đéc", "Tam Nông", "Tân Hồng", "Thanh Bình", "Tháp Mười"]
    ]
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var thumbnail: CustomCircleImageView!
    @IBOutlet weak var idTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var pwTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var pwCheckTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var firstNameTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var lastNameTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var emailTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var phoneNumberPickerView: UnderlinePickerView!
    @IBOutlet weak var phoneNumberTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var genderRadioButtonViewButton: CustomRadioButtonView!
    @IBOutlet weak var notSendBirthday: UIButton!
    @IBOutlet weak var birthdayDatePicker: CustomDatePicker!
    @IBOutlet weak var mainAddressTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var subAddressTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var countryPickerView: UnderlinePickerView!
    @IBOutlet weak var provincePickerView: UnderlinePickerView!
    @IBOutlet weak var cityPickerView: UnderlinePickerView!
    @IBOutlet weak var zipCodeTextField: UnderlineTextFieldBlack!
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBAction func checkboxButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    var isFaceBook: Bool! = false
    var passIdInvalid: Bool! = false
    var fbToken: String!
    
    var isEdit: Bool! = false
    
    @IBAction func checkIdInavlid(_ sender: UIButton) {
        if !isFaceBook {
            Alamofire.request(
                Constants.BASE_URL + "User_Chk_Id.jsp",
                method: .post,
                parameters: ["id": idTextField.text!],
                encoding: URLEncoding.default,
                headers: nil
                )
                .validate(statusCode: 200..<300)
                .responseObject {(response: DataResponse<ResFind>) in
                    if let result = response.result.value {
                        if(result.error_msg == "duplicate") {
                            self.showAlert(title: NSLocalizedString("id_duplicated", comment: ""))
                        } else {
                            self.passIdInvalid = true
                            self.checkButton.setTitle("OK", for: .normal)
                        }
                    }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setWidgets()
        
        Analytics.logEvent("SignUp", parameters: [
            "signup": "start" as NSObject
            ])
    }
    
    func setWidgets() {
        cancelButton.setTitle("CANCEL", for: .normal)
        cancelButton.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        idTextField.placeholder = NSLocalizedString("id_hint", comment: "")
        checkButton.setTitle("check", for: .normal)
        pwTextField.placeholder = NSLocalizedString("pw_hint", comment: "")
        pwCheckTextField.placeholder =  NSLocalizedString("pw_hint_confirm", comment: "")
        firstNameTextField.placeholder = NSLocalizedString("first_name", comment: "")
        lastNameTextField.placeholder = NSLocalizedString("last_name", comment: "")
        emailTextField.placeholder = NSLocalizedString("email_hint", comment: "")
        
        phoneNumberPickerView.setInit()
        phoneNumberPickerView.pickerView.delegate = self
        phoneNumberPickerView.pickerView.dataSource = self
        phoneNumberPickerView.label.text = countryCodes.first
        phoneNumberTextField.placeholder = NSLocalizedString("phone_hint", comment: "")
        genderRadioButtonViewButton.setInit(title: "Gender", titles: ["Male", "Female", "Other"])
        notSendBirthday.imageView?.contentMode = .scaleAspectFit
        birthdayDatePicker.setInit()
        mainAddressTextField.placeholder = NSLocalizedString("address1", comment: "")
        subAddressTextField.placeholder = NSLocalizedString("address2", comment: "")
        
        countryPickerView.setInit()
        countryPickerView.pickerView.delegate = self
        countryPickerView.pickerView.dataSource = self
        countryPickerView.label.text = Country.allCases[0].localized
        
        provincePickerView.setInit()
        provincePickerView.pickerView.delegate = self
        provincePickerView.pickerView.dataSource = self
        provincePickerView.label.text = provinces.first

        cityPickerView.setInit()
        cityPickerView.pickerView.delegate = self
        cityPickerView.pickerView.dataSource = self
        cityPickerView.label.text = cities.first?.first
        
        zipCodeTextField.placeholder = NSLocalizedString("zipcode", comment: "")
        zipCodeTextField.keyboardType = .numberPad

        privacyButton.imageView?.contentMode = .scaleAspectFit
        notificationButton.imageView?.contentMode = .scaleAspectFit
        
        if isFaceBook {
            if let id = Constants.USER_DEFAULT.value(forKey: "id") as? String {
                idTextField.text = id
            }
            passIdInvalid = true
            
            if let loggedInToken = AccessToken.current {
                GraphRequest(graphPath: "me",
                             parameters: ["fields": "id, email, first_name, last_name"]).start { (response, results) -> Void in
                                
                                switch results {
                                case .failed:
                                    break;
                                case .success(let response):
                                    guard let id = response.dictionaryValue?["id"] as? String,
                                        let firstName = response.dictionaryValue?["first_name"] as? String, let lastName = response.dictionaryValue?["last_name"] as? String else {
                                            
                                            return
                                    }
                                    
                                    // 사용자 정보 조회 성공!
                                    self.idTextField.text = id
                                    //                                    self.emailTextField.textField.text = email
                                    self.firstNameTextField.text = firstName
                                    self.lastNameTextField.text = lastName
                                    self.fbToken = loggedInToken.authenticationToken
                                }
                }
            }
            
            idTextField.isEnabled = false
            checkButton.isHidden = true
            pwTextField.text = "facebook"
            pwTextField.isEnabled = false
            pwCheckTextField.text = "facebook"
            pwCheckTextField.isEnabled = false
            firstNameTextField.isEnabled = false
            lastNameTextField.isEnabled = false
        }
        
        if isEdit {
            idTextField.isEnabled = false
            passIdInvalid = true
            checkButton.isHidden = true
            firstNameTextField.isEnabled = false
            lastNameTextField.isEnabled = false
            
            Alamofire.request(
                Constants.BASE_URL + "User_Info_Ok.jsp",
                method: .post,
                parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn") ?? ""],
                encoding: URLEncoding.default,
                headers: nil
                )
                .validate(statusCode: 200..<300)
                .responseObject {(response: DataResponse<ResUserInfo>) in
                    if let result = response.result.value {
                        if(result.error_msg == "") {
                            self.idTextField.text = result.id
                            self.firstNameTextField.text = result.first_name
                            self.lastNameTextField.text = result.last_name
                            self.emailTextField.text = result.email
                            if result.phone_num != "" {
                                self.phoneNumberTextField.text = String(result.phone_num.split(separator: " ")[1])
                            }
                            
                            switch result.gender {
                            case "M":
                                (self.genderRadioButtonViewButton.buttons[0]).sendActions(for: .touchUpInside)
                                break
                            case "F":
                                (self.genderRadioButtonViewButton.buttons[1]).sendActions(for: .touchUpInside)
                                break
                            case "O":
                                (self.genderRadioButtonViewButton.buttons[2]).sendActions(for: .touchUpInside)
                                break
                            case .none:
                                break
                            case .some(_):
                                break
                            }
                            
                            let dateString = result.birthday
                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyyMMdd"
                            let date = dateFormatter.date(from: dateString!)
                            
                            self.birthdayDatePicker.datePicker.setDate(date!, animated: false)
                            
                            self.mainAddressTextField.text = result.address1
                            self.subAddressTextField.text = result.address2
                            
                            var i: Int = 0
                            
                            for s in self.provinces {
                                if s == result.province {
                                    self.provincePickerView.pickerView.selectRow(i, inComponent: 0, animated: false)
                                    self.pickerView(self.provincePickerView.pickerView, didSelectRow: i, inComponent: 0)
                                    break
                                }
                                i += 1
                            }
                            
                            if i == 63 {
                                i = 0
                            }
                            
                            var j: Int = 0
                            
                            for s in self.cities[i] {
                                if s == result.city {
                                    self.cityPickerView.pickerView.selectRow(j, inComponent: 0, animated: false)
                                    self.pickerView(self.cityPickerView.pickerView, didSelectRow: j, inComponent: 0)
                                }
                                j += 1
                            }
                            
                            
                            self.zipCodeTextField.text = result.zip_code
                            self.privacyButton.sendActions(for: .touchUpInside)
                        } else {
                            print(result.error_msg)
                        }
                    }
            }
        }
    }
    
    @IBAction func handleCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    func checkString(newText:String, filter:String = "^[a-zA-Z0-9!@#$%^&*()_+]{6,16}$") -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", filter)
        return predicate.evaluate(with: newText)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if !passIdInvalid {
            showAlert(title: NSLocalizedString("id_duplicated", comment: ""))
            return
        }
        
        guard let id = idTextField.text, !id.isEmpty, checkString(newText: idTextField.text!) else {
            showAlert(title: NSLocalizedString("id_invalid", comment: ""))
            return
        }
        
        guard let pw = pwTextField.text, pw == pwCheckTextField.text, checkString(newText: pwTextField.text!) else {
            showAlert(title: NSLocalizedString("pw_invalid", comment: ""))
            return
        }
        
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else {
            showAlert(title: NSLocalizedString("first_name_error", comment: ""))
            return
        }
        guard let lastName = lastNameTextField.text, !lastName.isEmpty else {
            showAlert(title: NSLocalizedString("last_name_error", comment: ""))
            return
        }
        guard let email = emailTextField.text, !email.isEmpty else {
            showAlert(title: NSLocalizedString("email_hint_error", comment: ""))
            return
        }
        guard let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty else {
            showAlert(title: NSLocalizedString("email_hint_error", comment: ""))
            return
        }
        guard let gender = genderRadioButtonViewButton.selectedButton?.titleLabel?.text?.first else {
            showAlert(title: NSLocalizedString("gender_error", comment: ""))
            return
        }
        guard let address1 = mainAddressTextField.text else {
            showAlert(title: NSLocalizedString("address1_error", comment: ""))
            return
        }
        guard let address2 = subAddressTextField.text else {
            showAlert(title: NSLocalizedString("address2_error", comment: ""))
            return
        }
        guard let zipcode = zipCodeTextField.text else {
            showAlert(title: NSLocalizedString("zipcode_error", comment: ""))
            return
        }
        guard privacyButton.isSelected else {
            showAlert(title: NSLocalizedString("privacy_policy_error", comment: ""))
            return
        }
        
        let parameters: [String: Any] = [
            "id": id,
            "pw": pw,
            "email": email,
            "first_name": firstName,
            "last_name": lastName,
            "gender": gender,
            "birthday": notSendBirthday.isSelected ? "19700101" : birthdayDatePicker.datePicker.date.stringFormat,
            "phone_num": countryCodes[countryPickerView.pickerView.selectedRow(inComponent: 0)] + " " + phoneNumber,
            "country": Country.allCases[countryPickerView.pickerView.selectedRow(inComponent: 0)].localized,
            "province": provinces[provincePickerView.pickerView.selectedRow(inComponent: 0)],
            "city": cities[provincePickerView.pickerView.selectedRow(inComponent: 0)][cityPickerView.pickerView.selectedRow(inComponent: 0)],
            "address1": address1,
            "address2": address2,
            "zip_code": zipcode
        ]
        
        if !isEdit {
            NetworkAPI.requestWith(endUrl: "User_Insert_Ok.jsp", imageData: nil, parameters: parameters, onCompletion: { (response) in
                if let result = response.result.value {
                    if(result.error_msg == "id error") {
                        
                    } else if(result.error_msg == "pw error") {
                        
                    } else if(result.error_msg == "ok") {
                        if self.isFaceBook {
                            Alamofire.request(
                                Constants.BASE_URL + "User_Insert_Token_Ok.jsp",
                                method: .post,
                                parameters: ["id": self.idTextField.text!,
                                             "fb_token": String(self.fbToken)],
                                encoding: URLEncoding.default,
                                headers: nil
                                )
                                .validate(statusCode: 200..<300)
                                .responseObject {(response: DataResponse<ResFind>) in
                                    if let result = response.result.value {
                                        Constants.USER_DEFAULT.setValue(true, forKey: "fb_login")
                                        Constants.USER_DEFAULT.setValue(self.idTextField.text, forKey: "id")
                                        Constants.USER_DEFAULT.setValue(self.fbToken, forKey: "tkn")
                                    }
                            }
                        } else {
                            Constants.USER_DEFAULT.setValue(false, forKey: "fb_login")
                        }
                        
                        Analytics.logEvent("SignUp", parameters: [
                            "signup": "finish" as NSObject
                            ])
                        
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        print(result.error_msg)
                    }
                }
            }) { (error) in
                
            }
        } else {
            let parameters2: [String: Any] = [
                "token": Constants.USER_DEFAULT.string(forKey: "tkn")!,
                "email": email,
                "first_name": firstName,
                "last_name": lastName,
                "gender": gender,
                "birthday": notSendBirthday.isSelected ? "19700101" : birthdayDatePicker.datePicker.date.stringFormat,
                "phone_num": countryCodes[countryPickerView.pickerView.selectedRow(inComponent: 0)] + " " + phoneNumber,
                "country": Country.allCases[countryPickerView.pickerView.selectedRow(inComponent: 0)].localized,
                "province": provinces[provincePickerView.pickerView.selectedRow(inComponent: 0)],
                "city": cities[provincePickerView.pickerView.selectedRow(inComponent: 0)][cityPickerView.pickerView.selectedRow(inComponent: 0)],
                "address1": address1,
                "address2": address2,
                "zip_code": zipcode
            ]
            
            if pw != "" {
                Alamofire.request(
                    Constants.BASE_URL + "User_UpdatePw_Ok.jsp",
                    method: .post,
                    parameters: ["token": Constants.USER_DEFAULT.string(forKey: "tkn")!,
                                 "pw": pw],
                    encoding: URLEncoding.default,
                    headers: nil
                    )
                    .validate(statusCode: 200..<300)
                    .responseObject {(response: DataResponse<ResFindPw>) in
                        if let result = response.result.value {
                            if(result.error_msg == "ok") {
                                
                            } else {
                                
                            }
                        }
                }
            }
            
            NetworkAPI.requestWith(endUrl: "User_Update_Token.jsp", imageData: nil, parameters: parameters2, onCompletion: { (response) in
                if let result = response.result.value {
                    if(result.error_msg == "id error") {
                        
                    } else if(result.error_msg == "pw error") {
                        
                    } else if(result.error_msg == "ok") {
                        
                        
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        print(result.error_msg)
                    }
                }
            }) { (error) in
                
            }
        }
    }
    
    func showAlert(title: String) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("done", comment: ""), style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
    
    
}

extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case phoneNumberPickerView.pickerView:
            return 1
        case countryPickerView.pickerView:
            return 1
        case provincePickerView.pickerView:
            return 1
        case cityPickerView.pickerView:
            return 1
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case phoneNumberPickerView.pickerView:
            return countryCodes.count
        case countryPickerView.pickerView:
            return Country.allCases.count
        case provincePickerView.pickerView:
            return provinces.count
        case cityPickerView.pickerView:
            return cities[provincePickerView.pickerView.selectedRow(inComponent: 0)].count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case phoneNumberPickerView.pickerView:
            return countryCodes[row]
        case countryPickerView.pickerView:
            return Country.allCases[row].localized
        case provincePickerView.pickerView:
            return provinces[row]
        case cityPickerView.pickerView:
            return cities[provincePickerView.pickerView.selectedRow(inComponent: 0)][row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case phoneNumberPickerView.pickerView:
            phoneNumberPickerView.label.text = countryCodes[row]
        case countryPickerView.pickerView:
            countryPickerView.label.text = Country.allCases[row].localized
        case provincePickerView.pickerView:
            provincePickerView.label.text = provinces[row]
            cityPickerView.pickerView.selectRow(0, inComponent: 0, animated: false)
            cityPickerView.label.text = cities[row][0]
        case cityPickerView.pickerView:
            cityPickerView.label.text = cities[provincePickerView.pickerView.selectedRow(inComponent: 0)][row]
        default:
            break
        }
    }
}
