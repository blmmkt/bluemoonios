//
//  SlidingViewController.swift
//  BluemooniOS
//
//  Created by Jinsoo Park on 19/04/2019.
//

import UIKit
import NavigationDrawer

class SlidingViewController: UIViewController {

    var interactor: Interactor? = nil
    
    @IBOutlet weak var scrollNav: UIScrollView!
    @IBOutlet weak var navHeader: UIView!
    var navItemHome: CustomNaviItem!
    var navItemComingSoon: CustomNaviItem!
    var navItemSale: CustomNaviItem!
    var navItemCart: CustomNaviItem!
    var navItemWish: CustomNaviItem!
    var navItemMyPage: CustomNaviItem!
    var navItemFaq: CustomNaviItem!
    var navItemSettings: CustomNaviItem!
    var navItemPolicy: CustomNaviItem!
    var navItemAbout: CustomNaviItem!
    
    var mainDelegate: SlidingDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setWidgets()
    }
    
    override func viewDidLayoutSubviews() {
        scrollNav.contentSize = CGSize.init(width: UIScreen.main.bounds.width * 0.8, height: navItemAbout.frame.origin.y + 100)
    }
    
    func setWidgets() {
//        scrollNav.isExclusiveTouch = false
        scrollNav.delaysContentTouches = false
        setNaviItems()
    }
    
    func setNaviItems() {
        setHomeButton()
        setComingSoonButton()
        setOnSale()
        setCart()
        setWish()
        setDivider1()
        setMyPage()
        setDivider2()
        setFaq()
        setSettings()
        setPolicy()
        setAbout()
    }
    
    func setHomeButton() {
        self.navItemHome = CustomNaviItem(frame: CGRect.init(x: 0, y: navHeader.frame.origin.y + 112, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemHome.setIcon(image: "icon_home")
        self.navItemHome.setTitle(title: "Home")
        self.navItemHome.addTarget(self, action: #selector(homeBtnPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemHome)
    }
    
    func setComingSoonButton() {
        self.navItemComingSoon = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemHome.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemComingSoon.setIcon(image: "icon_comingsoon")
        self.navItemComingSoon.setTitle(title: "Coming Soon")
        self.navItemComingSoon.addTarget(self, action: #selector(comingSoonPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemComingSoon)
    }
    
    func setOnSale() {
        self.navItemSale = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemComingSoon.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemSale.setIcon(image: "icon_sale")
        self.navItemSale.setTitle(title: "On Sale")
        self.navItemSale.addTarget(self, action: #selector(onSalePressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemSale)
    }
    
    func setCart() {
        self.navItemCart = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemSale.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemCart.setIcon(image: "icon_cart")
        self.navItemCart.setTitle(title: "Cart")
        self.navItemCart.addTarget(self, action: #selector(cartPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemCart)
    }
    
    func setWish() {
        self.navItemWish = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemCart.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemWish.setIcon(image: "icon_wish")
        self.navItemWish.setTitle(title: "Wish List")
        
        scrollNav.addSubview(navItemWish)
    }
    
    func setDivider1() {
        let divider1 = UIView()
        divider1.frame = CGRect.init(x: 0, y: navItemWish.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 1)
        divider1.backgroundColor = UIColor.lightGray
        
        scrollNav.addSubview(divider1)
    }
    
    func setMyPage() {
        self.navItemMyPage = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemWish.frame.origin.y + 73, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemMyPage.setIcon(image: "icon_profile")
        self.navItemMyPage.setTitle(title: "My Page")
        self.navItemMyPage.addTarget(self, action: #selector(myPagePressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemMyPage)
    }
    
    func setDivider2() {
        let divider2 = UIView()
        divider2.frame = CGRect.init(x: 0, y: navItemMyPage.frame.origin.y + 48, width: UIScreen.main.bounds.width * 0.8, height: 1)
        divider2.backgroundColor = UIColor.lightGray
        
        scrollNav.addSubview(divider2)
    }
    
    func setFaq() {
        self.navItemFaq = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemMyPage.frame.origin.y + 73, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemFaq.setIcon(image: "icon_faq")
        self.navItemFaq.setTitle(title: "FAQ")
        
        scrollNav.addSubview(navItemFaq)
    }
    
    func setSettings() {
        self.navItemSettings = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemFaq.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemSettings.setIcon(image: "icon_settings")
        self.navItemSettings.setTitle(title: "Settings")
        self.navItemSettings.addTarget(self, action: #selector(settingsPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemSettings)
    }

    func setPolicy() {
        self.navItemPolicy = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemSettings.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemPolicy.setIcon(image: "icon_policy")
        self.navItemPolicy.setTitle(title: "Privacy Policy")
        self.navItemPolicy.addTarget(self, action: #selector(policyPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemPolicy)
    }
    
    func setAbout() {
        self.navItemAbout = CustomNaviItem(frame: CGRect.init(x: 0, y: navItemPolicy.frame.origin.y + 60, width: UIScreen.main.bounds.width * 0.8, height: 36))
        self.navItemAbout.setIcon(image: "icon_about")
        self.navItemAbout.setTitle(title: "About Company")
        self.navItemAbout.addTarget(self, action: #selector(aboutPressed(_:)), for: .touchUpInside)
        
        scrollNav.addSubview(navItemAbout)
    }
    
    
    @IBAction func closeBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func homeBtnPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        let progress = MenuHelper.calculateProgress(translationInView: translation, viewBounds: view.bounds, direction: .Left)
        
        MenuHelper.mapGestureStateToInteractor(
            gestureState: sender.state,
            progress: progress,
            interactor: interactor){
                self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func comingSoonPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickComingSoon()
        }
    }
    
    @objc func onSalePressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickOnSale()
        }
    }
    
    @objc func cartPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickCart()
        }
    }
    
    @objc func wishPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickWish()
        }
    }
    
    @objc func myPagePressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickMyPage()
        }
    }
    
    @objc func faqPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickFaq()
        }
    }
    
    @objc func settingsPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickSettings()
        }
    }
    
    @objc func policyPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickPolicy()
        }
    }
    
    @objc func aboutPressed(_ sender: CustomNaviItem) {
        dismiss(animated: true) {
            self.mainDelegate?.clickAbout()
        }
    }
}
